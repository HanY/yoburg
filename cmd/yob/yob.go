package main

import (
	"fmt"
	"log"
	"os"
	"path"
	"runtime/pprof"

	"codeberg.org/HanY/yoburg/lib/compiler"
	"codeberg.org/HanY/yoburg/lib/scanner"
	"codeberg.org/HanY/yoburg/lib/stdlib"
	"codeberg.org/HanY/yoburg/lib/value"
	"codeberg.org/HanY/yoburg/lib/vm"
)

var animal = "Fox"

func funnyPuts(caller value.Value, val []value.Value) value.Value {
	if len(val) != 1 {
		panic("puts only accept one argument")
	}
	s := value.ToString(val[0])
	fmt.Printf("%s says: %s\n", animal, s)
	return value.VALUE_NIL
}

var FunnyPuts = &value.NativeFunction{
	Func:      funnyPuts,
	Name:      "puts",
	ParamList: value.ParamList{Arity: 1},
}

func main() {
	f, err := os.Create("cpu.prof")
	if err != nil {
		log.Fatal("could not create CPU profile: ", err)
	}
	defer f.Close() // error handling omitted for example
	if err := pprof.StartCPUProfile(f); err != nil {
		log.Fatal("could not start CPU profile: ", err)
	}
	defer pprof.StopCPUProfile()

	fi, err := os.Open("main.yob")
	if err != nil {
		panic(err)
	}
	s := scanner.NewScanner(fi)
	pwd, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	c := compiler.NewCompiler(s, path.Join(pwd, "main.yob"))
	ret := c.Compile()
	vm.PrintChunk(ret.Chunk, *ret.InternedString)

	v := vm.NewVM(ret.Function,
		*ret.InternedString,
		*ret.StringConstMap,
		*ret.DebugInfo)

	blockFn := func(caller value.Value, val []value.Value) value.Value {
		// string, animal
		if len(val) != 2 {
			panic("wrong number of arguments")
		}
		animal = val[0].Payload.(string)
		v.DefineNative("puts", FunnyPuts)
		block := val[1]

		if block.Type != value.VAL_FUNC && block.Type != value.VAL_CLOSURE {
			panic("blockFn expect block")
		}

		v.InvokeBlock(block.Payload, nil)
		v.DefineNative("puts", stdlib.Puts)
		return value.VALUE_NIL
	}

	var blockNative = &value.NativeFunction{
		Func: blockFn,
		Name: "blockFun",
		ParamList: value.ParamList{Arity: 0,
			Block:    true,
			Keywords: []value.Keyword{{Name: "animal", DefaultVal: value.StringVal("Fox")}}},
	}

	v.DefineNative("puts", stdlib.Puts)
	v.DefineClass("Time", stdlib.Time)
	v.DefineClass("ENV", stdlib.ENV)
	v.DefineNative("blockFun", blockNative)
	v.Run()
}
