package compiler

import "codeberg.org/HanY/yoburg/lib/common_error"

func (c *Compiler) error(m string) {
	panic(common_error.SyntaxError{Message: m, Filename: c.Filename, Line: c.p.previousLine})
}
