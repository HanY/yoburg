package compiler

import (
	"fmt"
	"math"
	"os"

	"codeberg.org/HanY/yoburg/lib/opcode"
	"codeberg.org/HanY/yoburg/lib/scanner"
	"codeberg.org/HanY/yoburg/lib/token"
	"codeberg.org/HanY/yoburg/lib/util"
	"codeberg.org/HanY/yoburg/lib/value"
)

var debug = len(os.Getenv("YOB_DEBUG")) > 0

func isConstant(b byte) bool {
	return b >= 'A' && b <= 'Z'
}

type local struct {
	name       string
	isCaptured bool
}

type upvalue struct {
	index   uint8
	isLocal bool
}

type parser struct {
	s            *scanner.Scanner
	previous     token.Token
	current      token.Token
	previousLine int
	currentLine  int
}

type class struct {
	enclosing *class
}

type Compiler struct {
	p              *parser
	f              *value.Function
	locals         []local
	upvalues       []upvalue
	stringConstMap *map[string]int
	internedString *[]string
	debugInfos     *map[*value.Function]value.DebugInfo
	value.DebugInfo
	enclosing *Compiler
	className string
	class     *class
	returnOp  uint8
	closure   bool
}

type CompileResult struct {
	*value.Function
	InternedString *[]string
	StringConstMap *map[string]int
	DebugInfo      *map[*value.Function]value.DebugInfo
}

type ParseFunc func(c *Compiler, canAssign bool)

func newParser(s *scanner.Scanner) *parser {
	return &parser{s: s}
}

func NewCompiler(s *scanner.Scanner, fileName string) *Compiler {
	return NewCompilerWithDefault(s,
		fileName,
		&map[string]int{},
		&[]string{},
		&map[*value.Function]value.DebugInfo{})
}

func NewCompilerWithDefault(s *scanner.Scanner,
	fileName string,
	stringConstMap *map[string]int,
	internedString *[]string,
	debugInfos *map[*value.Function]value.DebugInfo) *Compiler {
	c := &Compiler{p: newParser(s),
		f:              value.NewFunction("::top"),
		stringConstMap: stringConstMap,
		internedString: internedString,
		debugInfos:     debugInfos,
		class:          &class{},
		returnOp:       opcode.OP_RETURN}
	c.addLocal("self")
	c.Filename = fileName
	return c
}

func (c *Compiler) inheritCompiler(fnName string) *Compiler {
	ret := &Compiler{p: c.p,
		f:              value.NewFunction(fnName),
		stringConstMap: c.stringConstMap,
		internedString: c.internedString,
		debugInfos:     c.debugInfos,
		enclosing:      c,
		className:      c.className,
		returnOp:       opcode.OP_RETURN}
	ret.Filename = c.Filename
	return ret
}

func (c *Compiler) inheritClassCompiler(className string) *Compiler {
	ret := &Compiler{p: c.p,
		f:              value.NewFunction(fmt.Sprintf("class:%s", className)),
		stringConstMap: c.stringConstMap,
		internedString: c.internedString,
		debugInfos:     c.debugInfos,
		enclosing:      c,
		className:      className,
		class:          &class{enclosing: c.class},
		returnOp:       opcode.OP_RETURN}
	ret.Filename = c.Filename
	return ret
}

func (p *parser) advance() {
	p.previous = p.current
	p.previousLine = p.currentLine
	p.current = p.s.Scan()
	p.currentLine = p.s.Line()
	if debug {
		token.PrintToken(p.current)
		fmt.Printf("LINE: %d\n", p.currentLine)
	}
}

func (c *Compiler) consume(t uint8) {
	if c.p.check(t) {
		c.p.advance()
		return
	}
	c.error(fmt.Sprintf("expect %v, got %v", t, c.p.current))
}

func (p *parser) check(t uint8) bool {
	return p.current.Type == t
}

func (p *parser) match(t uint8) bool {
	if p.check(t) {
		p.advance()
		return true
	}
	return false
}

// isBlock: is this compiler for a ruby do..end block
// don't confuse with isBlockend which means syntax block
func (c *Compiler) isBlock() bool {
	return c.returnOp == opcode.OP_RETURN_BLOCK
}

func (c *Compiler) loadCaller(line int) {
	if !c.closure {
		c.emit(opcode.OP_GET_LOCAL, line)
		c.emitUint8(0)
	} else {
		localIndex := c.resolveUpvalue("self")
		if localIndex < 0 {
			c.error("bug: can't find self in block")
		}
		c.emit(opcode.OP_GET_UPVALUE, line)
		c.emitUint8(uint8(localIndex))
	}
}

func (c *Compiler) loadBlock() {
	index := int(c.f.Arity) + len(c.f.Keywords) + 1 // plus one for block (block does not count as arity)
	c.emit(opcode.OP_GET_LOCAL, c.p.previousLine)
	c.emitUint8(uint8(index))
}

func (c *Compiler) findLocal(name string) int {
	for i, local := range c.locals {
		if local.name == name {
			return i
		}
	}
	return -1
}

func (c *Compiler) addLocal(name string) int {
	c.locals = append(c.locals, local{name: name})
	return len(c.locals)
}

func (c *Compiler) emit(code uint8, line int) {
	c.f.Chunk.Code = append(c.f.Chunk.Code, code)
	c.InsertDebugInfo(len(c.f.Code)-1, line)
}

func (c *Compiler) emitUint8(code uint8) {
	c.f.Chunk.Code = append(c.f.Chunk.Code, code)
}

func (c *Compiler) emitUint16(code uint16) {
	c.f.Chunk.Code = append(c.f.Chunk.Code, util.ToBytes2(code)...)
}

func (c *Compiler) emitUint32(code uint32) {
	c.f.Chunk.Code = append(c.f.Chunk.Code, util.ToBytes4(code)...)
}

func (c *Compiler) emitUint64(code uint64) {
	c.f.Chunk.Code = append(c.f.Chunk.Code, util.ToBytes8(code)...)
}

func (c *Compiler) emitJump(op uint8) int {
	c.emit(op, c.p.previousLine)
	c.emitUint8(0xff)
	c.emitUint8(0xff)
	return len(c.f.Chunk.Code) - 2
}

func (c *Compiler) patchJump(offset int) {
	jump := len(c.f.Chunk.Code) - 2 - offset
	if jump > math.MaxInt16 {
		c.error("too much code to jump")
	}
	codes := util.ToBytes2(uint16(jump))
	c.f.Chunk.Code[offset] = codes[0]
	c.f.Chunk.Code[offset+1] = codes[1]
}

func (c *Compiler) emitLoop(loopStart int) {
	c.emit(opcode.OP_LOOP, c.p.previousLine)
	offset := len(c.f.Chunk.Code) - loopStart + 2
	if offset > math.MaxInt16 {
		c.error("loop body too large")
	}
	c.emitUint16(uint16(offset))
}

func (c *Compiler) declaration() {
	isReturn := c.p.match(token.TOKEN_RETURN)
	line := c.p.previousLine
	if isReturn {
		if c.p.match(token.TOKEN_NEWLINE) {
			c.emit(opcode.OP_NIL, line)
		} else {
			c.expression()
			c.consume(token.TOKEN_NEWLINE)
		}
		c.emit(c.returnOp, line)
	} else {
		c.expression()
		c.p.match(token.TOKEN_NEWLINE)
		if !c.isBlockEnd() {
			c.emit(opcode.OP_POP, line)
		}
	}
}

func (c *Compiler) isBlockEnd() bool {
	switch c.p.current.Type {
	case token.TOKEN_END:
		fallthrough
	case token.TOKEN_ELSE:
		fallthrough
	case token.TOKEN_ELSIF:
		fallthrough
	case token.TOKEN_EOF:
		fallthrough
	case token.TOKEN_RIGHT_BRACE:
		fallthrough
	case token.TOKEN_RESCUE:
		return true
	}
	return false
}

func (c *Compiler) block() {
	for !c.isBlockEnd() {
		c.declaration()
	}
}

func (c *Compiler) expression() {
	c.parsePrecedence(PREC_ASSIGNMENT)
}

func (c *Compiler) parsePrecedence(precedence int) {
	c.p.advance()
	prefixRule := rules[c.p.previous.Type].prefix
	if prefixRule == nil {
		c.error("Expect expression")
	}
	start := len(c.f.Code)
	prefixRule(c, false)
	for precedence <= rules[c.p.current.Type].precedence {
		c.p.advance()
		switch c.p.previous.Type {
		case token.TOKEN_IF:
			ifUnlessInfixExpression(c, false, false, start)
		case token.TOKEN_UNLESS:
			ifUnlessInfixExpression(c, false, true, start)
		default:
			infixRule := rules[c.p.previous.Type].infix
			infixRule(c, false)
		}
	}
}

func grouping(c *Compiler, canAssign bool) {
	c.expression()
	c.consume(token.TOKEN_RIGHT_PAREN)
}

func unary(c *Compiler, canAssign bool) {
	opType := c.p.previous.Type
	line := c.p.previousLine
	c.parsePrecedence(PREC_UNARY)
	switch opType {
	case token.TOKEN_NEGATE_MINUS:
		fallthrough
	case token.TOKEN_MINUS:
		c.emit(opcode.OP_NEGATE, line)
	case token.TOKEN_BANG:
		c.emit(opcode.OP_NOT, line)
	}
}

func binary(c *Compiler, canAssign bool) {
	opType := c.p.previous.Type
	rule := rules[opType]
	line := c.p.previousLine
	c.parsePrecedence(rule.precedence + 1)
	switch opType {
	case token.TOKEN_PLUS:
		c.emit(opcode.OP_ADD, line)
	case token.TOKEN_NEGATE_MINUS:
		fallthrough
	case token.TOKEN_MINUS:
		c.emit(opcode.OP_SUB, line)
	case token.TOKEN_STAR:
		c.emit(opcode.OP_MUL, line)
	case token.TOKEN_SLASH:
		c.emit(opcode.OP_DIV, line)
	case token.TOKEN_GREATER_EQUAL:
		c.emit(opcode.OP_GREATER_EQUAL, line)
	case token.TOKEN_LESS_EQUAL:
		c.emit(opcode.OP_LESSER_EQUAL, line)
	case token.TOKEN_GREATER:
		c.emit(opcode.OP_LESSER_EQUAL, line)
		c.emit(opcode.OP_NOT, line)
	case token.TOKEN_LESS:
		c.emit(opcode.OP_GREATER_EQUAL, line)
		c.emit(opcode.OP_NOT, line)
	case token.TOKEN_EQUAL_EQUAL:
		c.emit(opcode.OP_EQUAL, line)
	case token.TOKEN_BANG_EQUAL:
		c.emit(opcode.OP_EQUAL, line)
		c.emit(opcode.OP_NOT, line)
	}
}

func (c *Compiler) internString(s string) uint32 {
	if ret, ok := (*c.stringConstMap)[s]; ok {
		return uint32(ret)
	}
	(*c.stringConstMap)[s] = len(*c.internedString)

	*c.internedString = append(*c.internedString, s)
	index := len(*c.internedString) - 1
	if index > math.MaxUint32 {
		c.error("too many strings")
	}
	return uint32(index)
}

func (c *Compiler) addConstant(v value.Value) uint16 {
	c.f.Constants = append(c.f.Constants, v)
	index := len(c.f.Constants) - 1
	if index > math.MaxUint16 {
		c.error("too many functions")
	}
	return uint16(index)
}

func (c *Compiler) loadString(index uint32, line int) {
	if index <= math.MaxUint8 {
		c.emit(opcode.OP_LOAD_STRING_8, line)
		c.emitUint8(uint8(index))
	} else if index <= math.MaxUint16 {
		c.emit(opcode.OP_LOAD_STRING_16, line)
		c.emitUint16(uint16(index))
	} else {
		c.emit(opcode.OP_LOAD_STRING_32, line)
		c.emitUint32(index)
	}
}

func (c *Compiler) loadConstant(index uint16, line int) {
	if index <= math.MaxUint8 {
		c.emit(opcode.OP_LOAD_CONST, line)
		c.emitUint8(uint8(index))
	} else {
		c.emit(opcode.OP_LOAD_CONST_U16, line)
		c.emitUint16(uint16(index))
	}
}

func intConstant(c *Compiler, canAssign bool) {
	val := c.p.previous.Value.(int)
	if val < math.MaxUint8 {
		c.emit(opcode.OP_INT_8, c.p.previousLine)
		c.emitUint8(uint8(val))
	} else if val < math.MaxUint16 {
		c.emit(opcode.OP_INT_16, c.p.previousLine)
	}
}

func doubleConstant(c *Compiler, canAssign bool) {
	val := c.p.previous.Value.(float64)
	c.emit(opcode.OP_DOUBLE, c.p.previousLine)
	c.emitUint64(math.Float64bits(val))
}

func stringConstant(c *Compiler, canAssign bool) {
	val := c.p.previous.Value.(string)
	index := c.internString(val)
	c.loadString(index, c.p.previousLine)
	if c.p.check(token.TOKEN_STRING) {
		c.p.advance()
		stringConstant(c, false)
		c.emit(opcode.OP_ADD, c.p.previousLine)
	}
}

func arrayExpression(c *Compiler, canAssign bool) {
	elementCount := 0
	line := c.p.previousLine
	for rules[c.p.current.Type].prefix != nil {
		elementCount++
		if elementCount > math.MaxUint8 {
			c.error("too many elements in array literal, max support 255")
		}
		c.expression()
		if c.p.match(token.TOKEN_COMMA) {
			c.p.match(token.TOKEN_NEWLINE)
		}
	}
	if !c.p.match(token.TOKEN_RIGHT_BRACKET) {
		c.error("expect ']' after array literal")
	}
	c.emit(opcode.OP_BUILD_ARRAY, line)
	c.emitUint8(uint8(elementCount))
}

func stringListExpression(c *Compiler, canAssign bool) {
	line := c.p.previousLine
	list := c.p.previous.Value.([]string)
	val := make([]value.Value, len(list))
	for i, e := range list {
		val[i] = value.StringVal(e)
	}
	c.loadConstant(c.addConstant(value.Value{Type: value.VAL_ARRAY, Payload: value.NewArrayFrom(val)}), line)
}

func doubleQuotedStringExpression(c *Compiler, canAssign bool) {
	concatenate := false
	for !c.p.check(token.TOKEN_EOF) &&
		!c.p.check(token.TOKEN_DOUBLE_QUOTE) {
		c.p.advance()
		switch c.p.previous.Type {
		case token.TOKEN_STRING:
			c.loadString(c.internString(c.p.previous.Value.(string)), c.p.previousLine)
			if concatenate {
				/* "foo#{exp}bar"
				        	 ^
						 need to concatenate bar
				*/

				c.emit(opcode.OP_ADD, c.p.previousLine)
			}
		case token.TOKEN_INTERPO_EXP_START:
			c.expression()
			c.consume(token.TOKEN_RIGHT_BRACE)
			info := value.CallInfo{Name: "to_s"}
			c.emit(opcode.OP_DOT_CALL, c.p.previousLine)
			c.f.AddCallInfo(info)
			if concatenate {
				// if "#{exp}", we don't need to concatenate anything
				c.emit(opcode.OP_ADD, c.p.previousLine)
			}
		default:
			c.error("invalid token in string")
		}
		concatenate = true
	}
	c.consume(token.TOKEN_DOUBLE_QUOTE)
	if c.p.check(token.TOKEN_DOUBLE_QUOTE) {
		c.p.advance()
		doubleQuotedStringExpression(c, false)
		c.emit(opcode.OP_ADD, c.p.previousLine)
	} else if c.p.check(token.TOKEN_STRING) {
		c.p.advance()
		stringConstant(c, false)
		c.emit(opcode.OP_ADD, c.p.previousLine)
	}
}

func subscriptExpression(c *Compiler, canAssign bool) {
	line := c.p.previousLine
	c.expression()
	c.consume(token.TOKEN_RIGHT_BRACKET)
	if c.p.match(token.TOKEN_EQUAL) {
		c.expression()
		c.emit(opcode.OP_SUBSCRIPT_SET, line)
	} else {
		c.emit(opcode.OP_SUBSCRIPT, line)
	}
}

func hashExpression(c *Compiler, canAssign bool) {
	pairCount := 0
	line := c.p.previousLine
	for rules[c.p.current.Type].prefix != nil {
		pairCount++
		if pairCount > math.MaxUint8 {
			c.error("too many keys in hash literal, max support 256")
		}
		c.expression()
		c.consume(token.TOKEN_ARROW)
		c.expression()
		if c.p.match(token.TOKEN_COMMA) {
			c.p.match(token.TOKEN_NEWLINE)
		}
	}
	if !c.p.match(token.TOKEN_RIGHT_BRACE) {
		c.error("expect '}' after hash literal")
	}
	c.emit(opcode.OP_BUILD_HASH, line)
	c.emitUint8(uint8(pairCount))
}

func symbolConstant(c *Compiler, canAssign bool) {
	val := string(c.p.previous.Value.([]byte))
	c.loadString(c.internString(val), c.p.previousLine)
}

func (c *Compiler) argList() value.CallInfo {
	leftParen := c.p.match(token.TOKEN_LEFT_PAREN)
	argCount := 0

	var callInfo value.CallInfo
	for c.p.current.Type != token.TOKEN_MINUS && rules[c.p.current.Type].prefix != nil {
		if c.p.current.Type == token.TOKEN_SYMBOL {
			c.p.advance()
			name := string(c.p.previous.Value.([]byte))
			if c.p.match(token.TOKEN_ARROW) {
				// keyword
				callInfo.Keys = append(callInfo.Keys, name)
				argCount-- // keyword does not count as arity
			} else {
				argCount++
				symbolConstant(c, false)
				goto PARAM_END
			}
		} else {
			if callInfo.Keys != nil {
				c.error("keyword parameter must appear at the end")
			}
			if argCount > math.MaxUint8 {
				c.error("too many params")
			}
		}

		argCount++
		c.expression()

	PARAM_END:
		// newline is allowed if we have paren
		// fn(exp1,
		//	  exp2)
		if c.p.match(token.TOKEN_COMMA) && leftParen {
			c.p.match(token.TOKEN_NEWLINE)
		}
	}

	if leftParen {
		c.consume(token.TOKEN_RIGHT_PAREN)
	}

	if c.p.match(token.TOKEN_DO) {
		// trailing block
		blockCompiler := c.inheritCompiler("block")
		blockCompiler.closure = true
		blockCompiler.returnOp = opcode.OP_RETURN_BLOCK
		if blockCompiler.p.match(token.TOKEN_BAR) {
			blockCompiler.f.ParamList = blockCompiler.parseParamDecl(token.TOKEN_BAR)
		}
		if !blockCompiler.p.match(token.TOKEN_NEWLINE) {
			c.error("expect newline after do")
		}

		blockCompiler.block()
		if !blockCompiler.p.match(token.TOKEN_END) {
			c.error("expect end after function")
		}

		c.loadFunction(blockCompiler)
		callInfo.Block = true
	}
	callInfo.Arity = uint8(argCount)

	return callInfo
}

func (c *Compiler) methodCall(name string, callType uint8) {
	line := c.p.previousLine
	start := len(c.f.Code)
	callInfo := c.argList()

	callInfo.Name = name
	c.emit(callType, line)
	c.f.AddCallInfo(callInfo)
	if callInfo.Block {
		c.f.AddExceptionReturnEntry(start, len(c.f.Code))
	}
}

func dotExpression(c *Compiler, canAssign bool) {
	_dotExpression(c, opcode.OP_DOT_CALL)
}

func optionalCallExpression(c *Compiler, canAssign bool) {
	_dotExpression(c, opcode.OP_OPTIONAL_CALL)
}

func _dotExpression(c *Compiler, callType uint8) {
	line := c.p.previousLine
	c.consume(token.TOKEN_IDENTIFIER)
	methodName := string(c.p.previous.Value.([]byte))
	if c.p.match(token.TOKEN_EQUAL) {
		methodName += "="
	}
	index := c.internString(methodName)
	if isConstant(methodName[0]) && !c.p.check(token.TOKEN_LEFT_PAREN) {
		c.loadString(index, line)
		c.emit(opcode.OP_LOAD_CLASS_CONST, line)
		return
	}

	c.methodCall(methodName, callType)

	/* optimization hack, if "A.new(arg)" called. We skip calling function new.
	If "initialize" is defined, we replace the *Class with newly created *Instance below
	"initialize" call frame base, and call initialize. After that we pop off
	the return value of initialize.
	So the *Instance we inserted become the return value.

	If "initialize" is not defined, we just replace the *Class with nealy created *Instance,
	and push a dummy value to be popped of by the below OP_POP
	*/
	if methodName == "new" {
		c.emit(opcode.OP_POP, c.p.previousLine)
	}
}

func ivarExpression(c *Compiler, canAssign bool) {
	name := string(c.p.previous.Value.([]byte))
	index := c.internString(name)
	line := c.p.previousLine
	if c.p.match(token.TOKEN_EQUAL) {
		c.expression()
		c.loadString(index, line)
		c.emit(opcode.OP_SET_IVAR, line)
		return
	}
	c.loadString(index, line)
	c.emit(opcode.OP_GET_IVAR, line)
}

func (c *Compiler) addUpvalue(index int, isLocal bool) int {
	c.f.UpvalueCount++
	for i, v := range c.upvalues {
		if v.index == uint8(index) && v.isLocal == isLocal {
			return i
		}
	}
	c.upvalues = append(c.upvalues, upvalue{index: uint8(index), isLocal: isLocal})
	if len(c.upvalues) > math.MaxUint8 {
		c.error("too many captured value in closure")
	}

	return len(c.upvalues) - 1
}

func (c *Compiler) resolveUpvalue(name string) int {
	if c.enclosing == nil {
		return -1
	}

	local := c.enclosing.findLocal(name)
	if local != -1 {
		c.enclosing.locals[local].isCaptured = true
		return c.addUpvalue(local, true)
	}

	local = c.enclosing.resolveUpvalue(name)
	if local != -1 {
		return c.addUpvalue(local, false)
	}

	return -1
}

func methodOrVarExpression(c *Compiler, canAssign bool) {
	name := string(c.p.previous.Value.([]byte))
	line := c.p.previousLine
	isConstant := isConstant(name[0])
	localIndex := c.findLocal(name)
	var isClosure bool
	if c.closure {
		if localIndex < 0 {
			localIndex = c.resolveUpvalue(name)
			if localIndex >= 0 {
				isClosure = true
			}
		}
	}

	if c.p.match(token.TOKEN_EQUAL) {
		c.expression()
		if isConstant {
			c.loadString(c.internString(name), line)
			c.emit(opcode.OP_SET_CLASS_CONST, line)
			return
		}

		if localIndex < 0 {
			localIndex = c.addLocal(name)
			if localIndex >= math.MaxUint8-1 {
				// variable assigning is an expression, max stack slots is variable count + 1
				c.error("too many local variables")
			}
			c.emit(opcode.OP_SET_LOCAL, line)
			c.emitUint8(uint8(len(c.locals)) - 1)
			return
		}

		if isClosure {
			c.emit(opcode.OP_SET_UPVALUE, line)
		} else {
			c.emit(opcode.OP_SET_LOCAL, line)
		}
		c.emitUint8(uint8(localIndex))
		return
	}

	if isConstant {
		// load current caller
		c.loadCaller(0)
		index := c.internString(name)
		c.loadString(index, line)
		c.emit(opcode.OP_LOAD_CLASS_CONST, line)
		return
	}

	if localIndex < 0 {
		// no local var found, assume method
		c.internString(name)

		// load current caller
		c.loadCaller(line)
		c.methodCall(name, opcode.OP_CALL)
	} else {
		if isClosure {
			c.emit(opcode.OP_GET_UPVALUE, line)
		} else {
			c.emit(opcode.OP_GET_LOCAL, line)
		}
		c.emitUint8(uint8(localIndex))
	}
}

func (c *Compiler) parseParamDecl(endToken uint8) value.ParamList {
	l := value.ParamList{}
	for !c.p.check(endToken) {
		isBlock := c.p.match(token.TOKEN_AND)
		if isBlock {
			l.Block = true
		} else if l.Block {
			c.error("block param goes last")
		}
		isVarArg := c.p.match(token.TOKEN_STAR)
		if isVarArg {
			if l.VarArg {
				c.error("can only declare one variable arg")
			} else if l.Keywords != nil {
				c.error("variable arg must be declared before keywords")
			} else if l.Block {
				c.error("variable arg must be declared before block")
			}
			l.VarArg = true
			// def foo(*)
			if c.p.match(endToken) {
				c.addLocal("")
				return l
			}
		}

		if !c.p.match(token.TOKEN_IDENTIFIER) {
			c.error("expect param name")
		}

		name := string(c.p.previous.Value.([]byte))
		localIndex := c.addLocal(name)
		if localIndex >= 255 {
			c.error("too many params")
		}

		if c.p.match(token.TOKEN_COLON) {
			// keyword args
			if isBlock {
				c.error("can't take block as keyword")
			}
			if !c.p.check(token.TOKEN_COMMA) && !c.p.check(endToken) {
				var val value.Value
				switch c.p.current.Type {
				case token.TOKEN_STRING:
					val = value.StringVal(c.p.current.Value.(string))
				case token.TOKEN_DOUBLE:
					val = value.Value{Type: value.VAL_DOUBLE, Payload: c.p.current.Value.(float64)}
				case token.TOKEN_INT:
					val = value.Value{Type: value.VAL_INT, Payload: c.p.current.Value.(int)}
				case token.TOKEN_NIL:
					val = value.VALUE_NIL
				case token.TOKEN_FALSE:
					val = value.VALUE_FALSE
				case token.TOKEN_TRUE:
					val = value.VALUE_TRUE
				default:
					c.error("default keyword arguments can only be literal")
				}
				l.Keywords = append(l.Keywords, value.Keyword{Name: name, DefaultVal: val})
				c.p.advance()
			} else {
				// def foo(bar:)
				// no default value
				l.Keywords = append(l.Keywords, value.Keyword{Name: name})
			}
		} else if !isBlock && !isVarArg {
			l.Arity++
		}
		c.p.match(token.TOKEN_COMMA)
		c.p.match(token.TOKEN_NEWLINE)
	}
	c.consume(endToken)
	return l
}

func (c *Compiler) loadFunction(compiler *Compiler) {
	fn := compiler.endCompiler()
	index := c.addConstant(value.Value{Type: value.VAL_FUNC, Payload: fn})
	line := c.p.previousLine
	if fn.UpvalueCount > 0 {
		c.emit(opcode.OP_CLOSURE, line)
		c.emitUint16(uint16(index))
		for i := uint8(0); i < fn.UpvalueCount; i++ {
			if compiler.upvalues[i].isLocal {
				c.emitUint8(1)
			} else {
				c.emitUint8(0)
			}
			c.emitUint8(compiler.upvalues[i].index)
		}
	} else {
		c.loadConstant(index, line)
	}
}

func (c *Compiler) function(compiler *Compiler) {
	compiler.addLocal("self")
	hasParen := compiler.p.match(token.TOKEN_LEFT_PAREN)
	if hasParen {
		compiler.f.ParamList = compiler.parseParamDecl(token.TOKEN_RIGHT_PAREN)
	}
	compiler.p.match(token.TOKEN_NEWLINE)

	compiler.block()
	if !compiler.p.match(token.TOKEN_END) {
		c.error("expect end after function")
	}
	c.loadFunction(compiler)
}

func methodDeclaration(c *Compiler, canAssign bool) {
	op := opcode.OP_METHOD
	if c.class.enclosing == nil {
		op = opcode.OP_GLOBAL_METHOD
	}

	var fnName string
	if c.p.match(token.TOKEN_IDENTIFIER) {
		fnName = string(c.p.previous.Value.([]byte))
	} else if c.p.match(token.TOKEN_LEFT_BRACKET) {
		c.consume(token.TOKEN_RIGHT_BRACKET)
		fnName = "[]"
	} else {
		c.error("expect function name")
	}

	// def self.foo
	if fnName == "self" {
		c.consume(token.TOKEN_DOT)
		c.consume(token.TOKEN_IDENTIFIER)
		op = opcode.OP_STATIC_METHOD
		fnName = string(c.p.previous.Value.([]byte))
	}

	if c.p.match(token.TOKEN_EQUAL) {
		// def a=(b)
		fnName = fnName + "="
	}

	line := c.p.previousLine
	fnIndex := c.internString(fnName)
	c.function(c.inheritCompiler(fnName))
	c.loadString(fnIndex, line)
	c.emit(op, line)
}

func globalVar(c *Compiler, canAssign bool) {
	name := string(c.p.previous.Value.([]byte))
	line := c.p.previousLine
	index := c.internString(name)
	if c.p.match(token.TOKEN_EQUAL) {
		c.expression()
		c.emit(opcode.OP_SET_GLOBAL, line)
	} else {
		c.emit(opcode.OP_GET_GLOBAL, line)
	}
	c.emitUint16(uint16(index))
}

func yieldExpression(c *Compiler, canAssign bool) {
	if !c.f.Block {
		c.f.Block = true
		// use of yield implcitly means func has accept block arg
		c.f.Arity++
	}
	line := c.p.previousLine
	start := len(c.f.Code)
	info := c.argList()
	c.loadBlock()
	c.emit(opcode.OP_CALL_BLOCK, line)
	c.emitUint8(info.Arity)
	c.f.AddExceptionBreakEntry(start, len(c.f.Code))
}

func ifExpression(c *Compiler, canAssign bool) {
	ifUnlessExpression(c, canAssign, false)
}

func unlessExpression(c *Compiler, canAssign bool) {
	ifUnlessExpression(c, canAssign, true)
}

func ifUnlessExpression(c *Compiler, canAssign bool, unless bool) {
	line := c.p.previousLine
	c.expression()
	if unless {
		c.emit(opcode.OP_NOT, line)
	}
	if !c.p.match(token.TOKEN_NEWLINE) {
		c.error("unexpected token after if")
	}
	ifJump := c.emitJump(opcode.OP_BRANCH_IF_FALSE)
	c.block()
	line = c.p.previousLine
	if len(c.f.Chunk.Code) == ifJump+3 {
		// empty if body, implicit nil
		c.emit(opcode.OP_NIL, line)
	}
	var endJumps []int
	endJumps = append(endJumps, c.emitJump(opcode.OP_JUMP))
	for c.p.match(token.TOKEN_ELSIF) {
		c.patchJump(ifJump)
		c.expression()
		if !c.p.match(token.TOKEN_NEWLINE) {
			c.error("unexpected token after elsif")
		}
		ifJump = c.emitJump(opcode.OP_BRANCH_IF_FALSE)
		c.block()
		line = c.p.previousLine
		if len(c.f.Chunk.Code) == ifJump+3 {
			// empty elsif body, implicit nil
			c.emit(opcode.OP_NIL, line)
		}

		endJumps = append(endJumps, c.emitJump(opcode.OP_JUMP))
	}
	c.patchJump(ifJump)
	elseBranch := len(c.f.Chunk.Code)
	if c.p.match(token.TOKEN_ELSE) {
		if !c.p.match(token.TOKEN_NEWLINE) {
			c.error("expect newline after else")
		} else {
			c.block()
		}
	}

	line = c.p.previousLine
	// emit nil if no else-branch or else-branch empty and if cond is false
	if len(c.f.Chunk.Code) == elseBranch {
		c.emit(opcode.OP_NIL, line)
	}
	for _, jump := range endJumps {
		c.patchJump(jump)
	}
	if !c.p.match(token.TOKEN_END) {
		c.error("expect end after if expression")
	}
}

func ifUnlessInfixExpression(c *Compiler, canAssign bool, unless bool, expStart int) {
	exp := make([]uint8, len(c.f.Code)-expStart)
	copy(exp, c.f.Code[expStart:])
	c.f.Code = c.f.Code[:expStart]

	c.expression()
	line := c.p.previousLine
	if unless {
		c.emit(opcode.OP_NOT, line)
	}
	ifJump := c.emitJump(opcode.OP_JUMP_IF_FALSE)
	c.emit(opcode.OP_POP, line)
	c.f.Code = append(c.f.Code, exp...)
	endJump := c.emitJump(opcode.OP_JUMP)
	c.patchJump(ifJump)
	c.emit(opcode.OP_POP, line)
	c.patchJump(endJump)
}

func falseExpression(c *Compiler, canAssign bool) {
	c.emit(opcode.OP_FALSE, c.p.previousLine)
}

func trueExpression(c *Compiler, canAssign bool) {
	c.emit(opcode.OP_TRUE, c.p.previousLine)
}

func nilExpression(c *Compiler, canAssign bool) {
	c.emit(opcode.OP_NIL, c.p.previousLine)
}

func andExpression(c *Compiler, canAssign bool) {
	jump := c.emitJump(opcode.OP_JUMP_IF_FALSE)
	c.emit(opcode.OP_POP, c.p.previousLine)
	c.parsePrecedence(PREC_AND)
	c.patchJump(jump)
}

func orExpression(c *Compiler, canAssign bool) {
	jump := c.emitJump(opcode.OP_JUMP_IF_TRUE)
	c.emit(opcode.OP_POP, c.p.previousLine)
	c.parsePrecedence(PREC_OR)
	c.patchJump(jump)
}

func whileExpression(c *Compiler, canAssign bool) {
	loopStart := len(c.f.Chunk.Code)
	c.expression()
	separator := c.p.match(token.TOKEN_DO)
	separator = c.p.match(token.TOKEN_NEWLINE) || separator
	if !separator {
		c.error("expect do or newline after while")
	}

	exitJump := c.emitJump(opcode.OP_JUMP_IF_FALSE)
	c.emit(opcode.OP_POP, c.p.previousLine)

	c.block()
	c.emitLoop(loopStart)
	c.patchJump(exitJump)
	c.emit(opcode.OP_POP, c.p.previousLine)
	// while expression's return value is always nil
	c.emit(opcode.OP_NIL, c.p.previousLine)
	if !c.p.match(token.TOKEN_END) {
		c.error("expect end after while")
	}
}

func rescueExpression(c *Compiler, canASsign bool) {
	_rescueExpression(c, 0, len(c.f.Code))
}

func _rescueExpression(c *Compiler, start int, end int) {
	rescueCompiler := c.inheritCompiler(fmt.Sprintf("%s:rescue", c.f.Name))
	rescueCompiler.locals = c.locals
	rescueCompiler.returnOp = opcode.OP_RETURN_BLOCK
	// rescue e => Exception
	if c.p.match(token.TOKEN_IDENTIFIER) {
		line := c.p.previousLine
		errName := string(c.p.previous.Value.([]byte))
		rescueCompiler.addLocal(errName)
		c.consume(token.TOKEN_ARROW)

		// check class type, rethrow if type mismatch
		rescueCompiler.emit(opcode.OP_DUP, line) // duplicate exception for class check
		rescueCompiler.emit(opcode.OP_GET_LOCAL, line)
		rescueCompiler.emitUint8(0)
		rescueCompiler.loadClassConstants(c.p.match(token.TOKEN_DOUBLE_COLON))
		rescueCompiler.emit(opcode.OP_CHECK_CLASS, line)
		rethrowJump := rescueCompiler.emitJump(opcode.OP_JUMP_IF_TRUE)
		rescueCompiler.emit(opcode.OP_POP, line)
		rescueCompiler.emit(opcode.OP_RAISE, line)
		rescueCompiler.patchJump(rethrowJump)
		rescueCompiler.emit(opcode.OP_POP, line)
	}
	c.consume(token.TOKEN_NEWLINE)
	rescueCompiler.block()
	f := rescueCompiler.endCompiler()
	// rescue block is actually a scope
	f.Code[len(f.Code)-1] = opcode.OP_RETURN_RESCUE
	c.f.AddExceptionRescueEntry(start, end, f)
}

func lambdaExpression(c *Compiler, canAssign bool) {
	compiler := c.inheritCompiler("lambda")
	compiler.closure = true
	if c.p.match(token.TOKEN_LEFT_PAREN) {
		compiler.f.ParamList = compiler.parseParamDecl(token.TOKEN_RIGHT_PAREN)
	}

	endToken := token.TOKEN_RIGHT_BRACE
	if compiler.p.match(token.TOKEN_DO) {
		compiler.p.match(token.TOKEN_NEWLINE)
		endToken = token.TOKEN_END
	} else {
		compiler.consume(token.TOKEN_LEFT_BRACE)
	}
	compiler.block()
	compiler.consume(endToken)
	c.loadFunction(compiler)
}

func beginExpression(c *Compiler, canASsign bool) {
	c.consume(token.TOKEN_NEWLINE)

	start := len(c.f.Code)
	c.block()
	line := c.p.previousLine
	if start == len(c.f.Code) {
		c.emit(opcode.OP_NIL, line)
	}
	end := len(c.f.Code)
	for c.p.match(token.TOKEN_RESCUE) {
		_rescueExpression(c, start, end)
		c.emit(opcode.OP_NOP, c.p.previousLine)
	}
	if !c.p.match(token.TOKEN_END) {
		c.error("expect end after `begin`")
	}
}

func doubleColonExpression(c *Compiler, canAssign bool) {
	c.loadClassConstants(true)
}

func (c *Compiler) loadClassConstants(root bool) {
	line := c.p.previousLine
	if root {
		// ::Foo
		c.emit(opcode.OP_GET_LOCAL, line)
		c.emitUint8(0)
		c.loadString(c.internString(""), line)
		c.emit(opcode.OP_LOAD_CLASS_CONST, line)
		if !c.p.check(token.TOKEN_IDENTIFIER) {
			// class Foo < :: is invalid
			c.error("expect constant after '::'")
		}
	}
	for c.p.match(token.TOKEN_IDENTIFIER) {
		c.loadString(c.internString(string(c.p.previous.Value.([]byte))), line)
		c.emit(opcode.OP_LOAD_CLASS_CONST, line)
		if !c.p.match(token.TOKEN_DOUBLE_COLON) {
			break
		}
	}
}

func classExpresion(c *Compiler, canAssign bool) {
	if !c.p.match(token.TOKEN_IDENTIFIER) {
		c.error("expect name after class")
	}
	className := string(c.p.previous.Value.([]byte))
	line := c.p.previousLine
	index := c.internString(className)

	compiler := c.inheritClassCompiler(className)
	if compiler.p.match(token.TOKEN_LESS) {
		compiler.emit(opcode.OP_GET_LOCAL, c.p.previousLine)
		compiler.emitUint8(0)
		compiler.loadClassConstants(c.p.match(token.TOKEN_DOUBLE_COLON))
		compiler.emit(opcode.OP_INHERIT, c.p.previousLine)
	}
	c.function(compiler)
	c.loadString(index, line)
	c.emit(opcode.OP_CLASS, line)
}

func raiseExpression(c *Compiler, canAssign bool) {
	line := c.p.previousLine
	if !c.p.check(token.TOKEN_NEWLINE) && !c.p.check(token.TOKEN_EOF) {
		c.expression()
		c.emit(opcode.OP_RAISE, line)
	} else {
		c.emit(opcode.OP_SIMPLE_RAISE, line)
	}
}

func requireRelativeExpression(c *Compiler, canAssign bool) {
	line := c.p.previousLine
	c.expression()
	c.emit(opcode.OP_REQUIRE_RELATIVE, line)
}

func (c *Compiler) endCompiler() *value.Function {
	line := c.p.previousLine
	// if function body is empty, we return nil, else we treat the last statement as expression
	if len(c.f.Code) == 0 {
		c.emit(opcode.OP_NIL, line)
	}
	c.emit(opcode.OP_RETURN, line)
	(*c.debugInfos)[c.f] = c.DebugInfo
	if c.isBlock() {
		c.f.AddExceptionNextEntry(0, len(c.f.Code))
	}
	c.f.LocalCount = uint8(len(c.locals))
	return c.f
}

func (c *Compiler) Compile() CompileResult {
	c.p.advance()
	c.p.match(token.TOKEN_NEWLINE)
	for !c.p.check(token.TOKEN_EOF) {
		c.declaration()
	}
	return CompileResult{c.endCompiler(), c.internedString, c.stringConstMap, c.debugInfos}
}
