package compiler

import "codeberg.org/HanY/yoburg/lib/token"

const (
	PREC_NONE       = iota
	PREC_ASSIGNMENT // =
	PREC_OR         // or
	PREC_AND        // and
	PREC_EQUALITY   // == !=
	PREC_COMPARISON // < > <= >=
	PREC_TERM       // + -
	PREC_FACTOR     // * /
	PREC_UNARY      // ! -
	PREC_CALL       // . ()
	PREC_PRIMARY
)

type ParseRule struct {
	prefix     ParseFunc
	infix      ParseFunc
	precedence int
}

var rules = make([]ParseRule, token.TOKEN_EOF+1)

func newParseRule(prefix ParseFunc, infix ParseFunc, precedence int) ParseRule {
	return ParseRule{prefix: prefix, infix: infix, precedence: precedence}
}

func init() {
	rules[token.TOKEN_LEFT_PAREN] = newParseRule(grouping, nil, PREC_NONE)
	rules[token.TOKEN_MINUS] = newParseRule(unary, binary, PREC_TERM)
	rules[token.TOKEN_NEGATE_MINUS] = newParseRule(unary, binary, PREC_TERM)
	rules[token.TOKEN_LEFT_BRACKET] = newParseRule(arrayExpression, subscriptExpression, PREC_EQUALITY)
	rules[token.TOKEN_LEFT_BRACE] = newParseRule(hashExpression, nil, PREC_TERM)
	rules[token.TOKEN_PLUS] = newParseRule(nil, binary, PREC_TERM)
	rules[token.TOKEN_STAR] = newParseRule(nil, binary, PREC_FACTOR)
	rules[token.TOKEN_SLASH] = newParseRule(nil, binary, PREC_FACTOR)
	rules[token.TOKEN_GREATER] = newParseRule(nil, binary, PREC_COMPARISON)
	rules[token.TOKEN_GREATER_EQUAL] = newParseRule(nil, binary, PREC_COMPARISON)
	rules[token.TOKEN_LESS] = newParseRule(nil, binary, PREC_COMPARISON)
	rules[token.TOKEN_LESS_EQUAL] = newParseRule(nil, binary, PREC_COMPARISON)
	rules[token.TOKEN_EQUAL_EQUAL] = newParseRule(nil, binary, PREC_EQUALITY)
	rules[token.TOKEN_BANG_EQUAL] = newParseRule(nil, binary, PREC_EQUALITY)
	rules[token.TOKEN_INT] = newParseRule(intConstant, nil, PREC_NONE)
	rules[token.TOKEN_DOUBLE] = newParseRule(doubleConstant, nil, PREC_NONE)
	rules[token.TOKEN_STRING] = newParseRule(stringConstant, nil, PREC_NONE)
	rules[token.TOKEN_DOUBLE_QUOTE] = newParseRule(doubleQuotedStringExpression, nil, PREC_NONE)
	rules[token.TOKEN_SYMBOL] = newParseRule(symbolConstant, nil, PREC_NONE)
	rules[token.TOKEN_BANG] = newParseRule(unary, nil, PREC_NONE)
	rules[token.TOKEN_IDENTIFIER_GLOBAL] = newParseRule(globalVar, nil, PREC_NONE)
	rules[token.TOKEN_IDENTIFIER] = newParseRule(methodOrVarExpression, nil, PREC_NONE)
	rules[token.TOKEN_STRING_LIST] = newParseRule(stringListExpression, nil, PREC_NONE)
	rules[token.TOKEN_IDENTIFIER_IVAR] = newParseRule(ivarExpression, nil, PREC_NONE)
	rules[token.TOKEN_YIELD] = newParseRule(yieldExpression, nil, PREC_NONE)
	rules[token.TOKEN_DOUBLE_COLON] = newParseRule(doubleColonExpression, nil, PREC_NONE)
	rules[token.TOKEN_IF] = newParseRule(ifExpression, nil, PREC_ASSIGNMENT)
	rules[token.TOKEN_UNLESS] = newParseRule(unlessExpression, nil, PREC_ASSIGNMENT)
	rules[token.TOKEN_TRUE] = newParseRule(trueExpression, nil, PREC_NONE)
	rules[token.TOKEN_FALSE] = newParseRule(falseExpression, nil, PREC_NONE)
	rules[token.TOKEN_NIL] = newParseRule(nilExpression, nil, PREC_NONE)
	rules[token.TOKEN_LOGIC_AND] = newParseRule(nil, andExpression, PREC_AND)
	rules[token.TOKEN_OR] = newParseRule(nil, orExpression, PREC_OR)
	rules[token.TOKEN_WHILE] = newParseRule(whileExpression, nil, PREC_NONE)
	rules[token.TOKEN_DEF] = newParseRule(methodDeclaration, nil, PREC_NONE)
	rules[token.TOKEN_CLASS] = newParseRule(classExpresion, nil, PREC_NONE)
	rules[token.TOKEN_DOT] = newParseRule(nil, dotExpression, PREC_CALL)
	rules[token.TOKEN_OPTIONAL_CALL] = newParseRule(nil, optionalCallExpression, PREC_CALL)
	rules[token.TOKEN_RAISE] = newParseRule(raiseExpression, nil, PREC_NONE)
	rules[token.TOKEN_BEGIN] = newParseRule(beginExpression, nil, PREC_NONE)
	rules[token.TOKEN_RESCUE] = newParseRule(rescueExpression, nil, PREC_NONE)
	rules[token.TOKEN_LAMBDA] = newParseRule(lambdaExpression, nil, PREC_NONE)
	rules[token.TOKEN_REQUIRE_RELATIVE] = newParseRule(requireRelativeExpression, nil, PREC_NONE)
}
