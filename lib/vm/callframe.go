package vm

import "codeberg.org/HanY/yoburg/lib/value"

type CallFrame struct {
	*value.Function
	UpValues   []value.UpValue
	pc         int
	stackStart int
}

func NewCallFrame(fn *value.Function, pc int, stackStart int) *CallFrame {
	return &CallFrame{fn, nil, pc, stackStart}
}
