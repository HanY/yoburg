package vm

import (
	"fmt"
	"unsafe"

	"codeberg.org/HanY/yoburg/lib/opcode"
	"codeberg.org/HanY/yoburg/lib/util"
	"codeberg.org/HanY/yoburg/lib/value"
)

type VM struct {
	stack           []value.Value
	callFrame       []CallFrame
	currentFrame    *CallFrame
	globals         map[string]value.Value
	globalFunc      map[string]value.Value
	openUpValues    *value.UpValue
	internedStrings []string
	stringConstMap  map[string]int
	debugInfos      map[*value.Function]value.DebugInfo
	classID         int
}

func NewVM(fn *value.Function,
	internedStrings []string,
	stringConstMap map[string]int,
	debugInfo map[*value.Function]value.DebugInfo) *VM {
	topLevel := value.NewClass("", 0)
	// class "" is top level, ::Foo refers to 'toplevel'->'Foo'
	topLevel.Constants[""] = value.Value{Type: value.VAL_CLASS, Payload: topLevel}
	topLevel.Constants["Exception"] = value.Value{Type: value.VAL_CLASS, Payload: ExceptionClass}
	i := topLevel.NewInstance()
	v := &VM{
		stack:           make([]value.Value, 1, 10000),
		callFrame:       make([]CallFrame, 0, 10000),
		globals:         make(map[string]value.Value),
		globalFunc:      make(map[string]value.Value),
		internedStrings: internedStrings,
		stringConstMap:  stringConstMap,
		debugInfos:      debugInfo,
		classID:         10}
	v.addCallFrame(fn, 0)
	v.stack[0] = value.Value{Type: value.VAL_INSTANCE, Payload: i}
	v.fillLocals(fn.LocalCount)
	return v
}

func (v *VM) DefineNative(name string, fn *value.NativeFunction) {
	v.globalFunc[name] = value.Value{Type: value.VAL_NATIVE, Payload: fn}
}

func (v *VM) DefineClass(name string, c *value.Class) {
	v.global().Class.Constants[name] = value.Value{Type: value.VAL_CLASS, Payload: c}
}

func (v *VM) Panic(m string) {
	err := RuntimeErrorClass.NewInstance()
	err.IVar["backtrace"] = stringArray(v.buildBacktrace())
	err.IVar["message"] = value.StringVal(m)
	panic(err)
}

func (v *VM) addCallFrame(fn *value.Function, stackStart int) {
	v.callFrame = v.callFrame[:len(v.callFrame)+1]
	v.callFrame[len(v.callFrame)-1].Function = fn
	v.callFrame[len(v.callFrame)-1].stackStart = stackStart
	v.callFrame[len(v.callFrame)-1].pc = 0
	v.callFrame[len(v.callFrame)-1].UpValues = nil
	v.currentFrame = &v.callFrame[len(v.callFrame)-1]
}

func (v *VM) global() *value.Instance {
	return v.stack[0].Payload.(*value.Instance)
}

func (v *VM) push(val value.Value) {
	v.stack = v.stack[:len(v.stack)+1]
	v.stack[len(v.stack)-1] = val
}

func (v *VM) pop() value.Value {
	val := v.stack[len(v.stack)-1]
	v.stack = v.stack[:len(v.stack)-1]
	return val
}

func (v *VM) stackTop() value.Value {
	return v.peek(0)
}

func (v *VM) peek(i int) value.Value {
	return v.stack[len(v.stack)-1-i]
}

func (v *VM) caller() value.Value {
	return v.stack[v.currentFrame.stackStart]
}

func (v *VM) advance() uint8 {
	code := v.currentFrame.Code[v.currentFrame.pc]
	v.currentFrame.pc++
	return code
}

func (v *VM) readUInt16() uint16 {
	start := v.currentFrame.pc
	v.currentFrame.pc += 2
	return util.FromBytes2(v.currentFrame.Code[start : start+2])
}

func (v *VM) readUInt32() uint32 {
	start := v.currentFrame.pc
	v.currentFrame.pc += 4
	return util.FromBytes4(v.currentFrame.Code[start : start+4])
}

func (v *VM) readUInt64() uint64 {
	start := v.currentFrame.pc
	v.currentFrame.pc += 8
	return util.FromBytes8(v.currentFrame.Code[start : start+8])
}

func (v *VM) fillLocals(count uint8) {
	count-- // self is already filled
	start := len(v.stack)
	v.stack = v.stack[:start+int(count)]
	for i := start; i < start+int(count); i++ {
		v.stack[i] = value.VALUE_NIL
	}
}

func (v *VM) defineMethod(global bool) {
	fnName := v.pop().Payload.(string)
	fn := v.stackTop()
	caller := v.caller()
	switch caller := caller.Payload.(type) {
	case *value.Class:
		caller.SetId(v.classID)
		v.classID++
		if fnName == "initialize" {
			caller.Initializer = fn.Payload.(*value.Function)
		} else {
			caller.Methods[fnName] = fn.Payload.(*value.Function)
		}
	case *value.Instance:
		if global {
			v.globalFunc[fnName] = value.Value{Type: value.VAL_FUNC, Payload: fn.Payload.(*value.Function)}
		} else {
			v.Panic("can't define method on instance")
		}
	default:
		v.Panic("bug")
	}
	v.stack[len(v.stack)-1] = value.Value{Type: value.VAL_STRING, Payload: fnName}
}

func (v *VM) defineStaticMethod() {
	fnName := v.pop().Payload.(string)
	fn := v.stackTop()
	caller := v.caller()
	switch caller.Type {
	case value.VAL_CLASS:
		if fnName == "initialize" {
			caller.Payload.(*value.Class).Initializer = fn.Payload.(*value.Function)
		} else {
			caller.Payload.(*value.Class).Constants[fnName] = fn
		}
	case value.VAL_INSTANCE:
		if caller.Payload.(*value.Instance) == v.global() {
			caller.Payload.(*value.Instance).Class.Methods[fnName] = fn.Payload.(*value.Function)
		} else {
			v.Panic("can't define static method on instance")
		}
	}
	v.stack[len(v.stack)-1] = value.Value{Type: value.VAL_STRING, Payload: fnName}
}

func findMethod(i *value.Instance, name string) (interface{}, bool) {
	c := i.Class
	for c != nil {
		f, ok := c.Methods[name]
		if ok {
			return f, true
		}
		c = c.SuperClass
	}
	return nil, false
}

func (v *VM) prepareArgs(info *value.CallInfo, p *value.ParamList, localCount uint8) int {
	stackOffset := 1 // 1 for self
	block := value.VALUE_NIL
	if info.Block {
		if !p.Block {
			v.Panic(fmt.Sprintf("function %s does not take block", info.Name))
		} else {
			block = v.pop()
		}
		stackOffset++
	}

	var keywordParams []value.Value
	if p.Keywords != nil {
		keywordParams = make([]value.Value, len(p.Keywords))
		for i, k := range p.Keywords {
			keywordParams[i] = k.DefaultVal
		}
		for len(info.Keys) > 0 {
			key := info.Keys[len(info.Keys)-1]
			info.Keys = info.Keys[:len(info.Keys)-1]
			val := v.pop()
			i := p.IndexKeyword(key)
			if i < 0 {
				v.Panic(fmt.Sprintf("no key named %s declared on %s", key, info.Name))
			}
			keywordParams[i] = val
		}
		stackOffset += len(p.Keywords)
	}

	var varArg value.Value
	if p.VarArg {
		count := int(info.Arity - p.Arity)
		varArgs := make([]value.Value, count)
		for i := count - 1; i >= 0; i-- {
			varArgs[i] = v.pop()
		}
		varArg = value.Value{Type: value.VAL_ARRAY, Payload: &value.Array{Data: varArgs}}
		stackOffset++
	}

	v.fillLocals(localCount - uint8(stackOffset) + 1) // plus 1 because fillLocals also ignore self

	if p.VarArg {
		v.push(varArg)
	}

	if p.Keywords != nil {
		v.stack = append(v.stack, keywordParams...)
	}

	if p.Block {
		v.push(block)
	}

	return len(v.stack) - int(p.Arity) - stackOffset
}

func (v *VM) blockCatch(callFrame int) {
	r := recover()
	if r == nil {
		return
	}
	val, ok := r.(value.Value)
	if !ok {
		panic(val)
	}
	if callFrame < 1 {
		v.Panic("can't return from here")
	}
	v.currentFrame = &v.callFrame[callFrame-1]
	stackStart := v.callFrame[callFrame].stackStart
	v.stack[stackStart] = val
	v.stack = v.stack[:stackStart+1]
	v.callFrame = v.callFrame[:callFrame]
}

/* InvokeBlock, fn parameter is either *value.Function or *value.Closure
 */
func (v *VM) InvokeBlock(fn interface{}, params []value.Value) value.Value {
	returnFrame := len(v.callFrame)
	switch fn := fn.(type) {
	case *value.Function:
		v.push(v.caller())
		v.stack = append(v.stack, params...)
		stackStart := len(v.stack) - len(params) - 1
		v.addCallFrame(fn, stackStart)
		v.fillLocals(fn.LocalCount)
	case *value.Closure:
		v.stack = append(v.stack, params...)
		stackStart := len(v.stack) - len(params) // closure doesn's have a caller
		v.addCallFrame(fn.Function, stackStart)
		v.callFrame[len(v.callFrame)-1].UpValues = fn.UpValues
		v.push(*v.callFrame[len(v.callFrame)-1].UpValues[0].Location)
		v.fillLocals(fn.LocalCount)
	}
	return v.run(returnFrame)
}

func (v *VM) callClosure(arity uint8, c *value.Closure) {
	if arity != c.Arity {
		v.Panic(fmt.Sprintf("wrong number of arguments, expect %d get %d", c.Arity, arity))
	}

	stackStart := len(v.stack) - int(arity) - 1

	if c.Keywords != nil || c.Block {
		for _, k := range c.Keywords {
			v.push(k.DefaultVal)
		}

		if c.Block {
			v.push(value.VALUE_NIL)
		}
	}

	v.addCallFrame(c.Function, stackStart)
	v.callFrame[len(v.callFrame)-1].UpValues = c.UpValues
	v.push(*c.UpValues[0].Location)
	v.fillLocals(c.LocalCount)
}

// callFuncSimple: only used by `yield`, does not have caller on stack
func (v *VM) callFuncSimple(arity uint8, b *value.Function) {
	if arity != b.Arity {
		v.Panic(fmt.Sprintf("wrong number of arguments, expect %d get %d", b.Arity, arity))
	}

	stackStart := len(v.stack) - int(arity) - 1

	if b.Keywords != nil || b.Block {
		for _, k := range b.Keywords {
			v.push(k.DefaultVal)
		}

		if b.Block {
			v.push(value.VALUE_NIL)
		}
	}

	v.addCallFrame(b, stackStart)
	v.fillLocals(b.LocalCount)
}

func (v *VM) callFunc(info *value.CallInfo, fn interface{}) {
	switch fn := fn.(type) {
	case *value.Function:
		if info.Arity != fn.Arity && !fn.VarArg {
			v.Panic(fmt.Sprintf("wrong number of arguments, expect %d get %d", fn.Arity, info.Arity))
		}
		stackStart := len(v.stack) - int(info.Arity) - 1
		if fn.NonSimpleArgs() {
			stackStart = v.prepareArgs(info, &fn.ParamList, fn.LocalCount)
		} else {
			v.fillLocals(fn.LocalCount)
		}
		v.addCallFrame(fn, stackStart)
	case *value.NativeFunction:
		if info.Arity != fn.Arity {
			v.Panic(fmt.Sprintf("wrong number of arguments, expect %d get %d", fn.Arity, info.Arity))
		}
		if fn.Block {
			defer v.blockCatch(len(v.callFrame) - 1)
		}
		stackStart := len(v.stack) - int(info.Arity)
		if fn.NonSimpleArgs() {
			stackStart = v.prepareArgs(info, &fn.ParamList, 0)
			stackStart++
		}
		v.stack[stackStart-1] = fn.Func(v.stack[stackStart-1], v.stack[stackStart:len(v.stack)])
		v.stack = v.stack[:stackStart]
	default:
		v.Panic("unknown func to call")
	}
}

func (v *VM) getGlobalFunc(name string, arity uint8) interface{} {
	fun, ok := v.globalFunc[name]
	if !ok {
		return nil
	}
	v.stack[len(v.stack)-1-int(arity)] = value.Value{Type: value.VAL_INSTANCE, Payload: v.global()}
	return fun.Payload
}

func (v *VM) opCall(globalFallback bool, optionalCall bool) {
	info := (*value.CallInfo)(unsafe.Pointer(&v.currentFrame.Code[v.currentFrame.pc]))
	v.currentFrame.pc += int(unsafe.Sizeof(*info))
	caller := v.peek(info.ArgCount())
	v._opCall(caller, info, globalFallback, optionalCall)
}

func (v *VM) _opCall(caller value.Value, info *value.CallInfo, globalFallback bool, optionalCall bool) {
	var fn interface{}
	var callerName string
	var callerId int
	switch caller := caller.Payload.(type) {
	case *value.Instance:
		callerName = caller.Name
		if info.Cache.Function != nil && caller.ID == info.Cache.ClassID {
			fn = info.Cache.Function
			if info.Cache.Global {
				v.stack[len(v.stack)-1-info.ArgCount()] = value.Value{Type: value.VAL_INSTANCE, Payload: v.global()}
			}
			break
		}
		_fn, ok := findMethod(caller, info.Name)
		if ok {
			fn = _fn
			info.Cache.Function = _fn
			info.Cache.ClassID = caller.Class.ID
		}
		callerId = caller.ID
	case *value.Class:
		callerName = caller.Name
		if info.Name == "new" {
			i := value.Value{Type: value.VAL_INSTANCE,
				Payload: caller.NewInstance()}
			if caller.Initializer == nil {
				if info.Arity != 0 {
					v.Panic(fmt.Sprintf("wrong number of args, expect 0 get %d", info.Arity))
				}
				v.stack[len(v.stack)-1] = i
				v.push(i) // dummy value to be popped off
				return
			}
			fn = caller.Initializer

			insertIndex := len(v.stack) - 2 - int(info.Arity) // insert return instance value below caller
			v.stack = append(v.stack[:insertIndex+1], v.stack[insertIndex:]...)
			v.stack[insertIndex+1] = i
			v.stack[insertIndex+2] = i
		} else {
			if info.Cache.Function != nil && caller.ID == info.Cache.ClassID {
				fn = info.Cache.Function
				if info.Cache.Global {
					v.stack[len(v.stack)-1-info.ArgCount()] = value.Value{Type: value.VAL_INSTANCE, Payload: v.global()}
				}
				break
			}
			class := caller
			for class != nil {
				constant, ok := class.Constants[info.Name]
				if ok {
					if constant.Type != value.VAL_FUNC && constant.Type != value.VAL_NATIVE {
						v.Panic(fmt.Sprintf("cannot find static method %s of %s", info.Name, caller.Name))
					}
					fn = constant.Payload
					info.Cache.ClassID = caller.ID
					info.Cache.Function = fn
					break
				}
				class = class.SuperClass
			}
		}
		callerId = caller.ID
	case *value.Hash:
		v.hashBuiltin(caller, info.Name, info.Arity)
		return
	case *value.Array:
		v.arrayBuiltin(caller, info.Name, info.Arity)
		return
	case string:
		v.stringBuiltin(caller, info.Name, info.Arity)
		return
	case int:
		v.intBuiltin(caller, info.Name, info.Arity)
		return
	case float64:
		v.doubleBuiltin(caller, info.Name, info.Arity)
		return
	case nil:
		if optionalCall {
			v.stack = v.stack[:len(v.stack)-int(info.Arity)]
			return
		}
		v.Panic(fmt.Sprintf("undefined method %s on nil", info.Name))
	case *value.Function:
		if info.Name == "call" {
			fn = caller
		} else {
			v.Panic(fmt.Sprintf("undefined method %s on Proc", info.Name))
		}
	case *value.Closure:
		if info.Name == "call" {
			v.callClosure(info.Arity, caller)
			return
		} else {
			v.Panic(fmt.Sprintf("undefined method %s on Proc", info.Name))
		}
	default:
		v.Panic("bug")
	}

	if fn == nil {
		if globalFallback {
			fn = v.getGlobalFunc(info.Name, uint8(info.ArgCount()))
		}
		if fn == nil {
			v.Panic(fmt.Sprintf("undefined local variable or method `%s' for %s", info.Name, callerName))
		}
		info.Cache.Function = fn
		info.Cache.ClassID = callerId
		info.Cache.Global = true
	}

	v.callFunc(info, fn)
}

func (v *VM) createClass(name string) value.Value {
	var c *value.Class
	caller := v.caller()
	switch caller := caller.Payload.(type) {
	case *value.Class:
		c = caller
	case *value.Instance:
		if caller == v.global() {
			c = caller.Class
		} else {
			v.Panic("can't define class in methods")
		}
	default:
		v.Panic("bug")
	}
	enclosedClass, ok := c.Constants[name]
	if !ok {
		newClass := value.NewClass(name, v.classID)
		v.classID++
		newClass.Enclosing = c
		enclosedClass = value.Value{Type: value.VAL_CLASS, Payload: newClass}
		c.Constants[name] = enclosedClass
		return enclosedClass
	}
	switch enclosedClass.Type {
	case value.VAL_CLASS:
		return enclosedClass
	default:
		v.Panic(fmt.Sprintf("Constant %s is not a class", name))
	}

	// unreachable
	return value.Value{}
}

func (v *VM) findClassConstant(name string) value.Value {
	var c *value.Class
	switch caller := v.stackTop().Payload.(type) {
	case *value.Class:
		c = caller
	case *value.Instance:
		c = caller.Class
	default:
		v.Panic(fmt.Sprintf("method undefined on %s", value.ToString(v.caller())))
	}

	for c != nil {
		val, ok := c.Constants[name]
		if ok {
			return val
		}
		c = c.Enclosing
	}
	v.Panic(fmt.Sprintf("undefined Constant: %s", name))
	// unreachable
	return value.Value{}
}

func (v *VM) opSubscript() {
	val := v.pop()
	switch v.stackTop().Type {
	case value.VAL_ARRAY:
		if val.Type != value.VAL_INT {
			v.Panic("can only subscript with int")
		}
		v.stack[len(v.stack)-1] = v.stackTop().Payload.(*value.Array).Data[val.Payload.(int)]
	case value.VAL_HASH:
		v.stack[len(v.stack)-1] = v.stackTop().Payload.(*value.Hash).Get(val)
	default:
		v.push(val)
		v._opCall(v.peek(1), &value.CallInfo{Name: "[]", Arity: 1}, false, false)
	}
}

func (v *VM) opSubscriptSet() {
	val := v.pop()
	key := v.pop()

	switch v.stackTop().Type {
	case value.VAL_ARRAY:
		if key.Type != value.VAL_INT {
			v.Panic("can only subscript with int")
		}
		v.stackTop().Payload.(*value.Array).Data[key.Payload.(int)] = val
		v.stack[len(v.stack)-1] = val
	case value.VAL_HASH:
		v.stackTop().Payload.(*value.Hash).Insert(key, val)
		v.stack[len(v.stack)-1] = val
	default:
		v.push(key)
		v.push(val)
		v._opCall(v.peek(2), &value.CallInfo{Name: "[]=", Arity: 2}, false, false)
	}
}

func toDouble(v value.Value) float64 {
	switch v.Type {
	case value.VAL_DOUBLE:
		return v.Payload.(float64)
	case value.VAL_INT:
		return float64(v.Payload.(int))
	}
	// unreachable
	return 0
}

func sub(rhs value.Value, lhs value.Value) value.Value {
	if lhs.Type == value.VAL_INT && rhs.Type == value.VAL_INT {
		return value.Value{Type: value.VAL_INT, Payload: lhs.Payload.(int) - rhs.Payload.(int)}
	} else {
		return value.Value{Type: value.VAL_DOUBLE, Payload: toDouble(lhs) - toDouble(rhs)}
	}
}

func (v *VM) add(rhs value.Value, lhs value.Value) value.Value {
	switch rhs.Type {
	case value.VAL_INT:
		if lhs.Type == value.VAL_INT {
			return value.Value{Type: value.VAL_INT, Payload: lhs.Payload.(int) + rhs.Payload.(int)}
		}
		fallthrough
	case value.VAL_DOUBLE:
		return value.Value{Type: value.VAL_DOUBLE, Payload: toDouble(lhs) + toDouble(rhs)}
	case value.VAL_ARRAY:
		if lhs.Type != value.VAL_ARRAY {
			v.Panic("can only add array to array")
		}
		r := rhs.Payload.(*value.Array)
		l := lhs.Payload.(*value.Array)
		return value.Value{Type: value.VAL_ARRAY, Payload: value.NewArrayFrom(append(l.Data, r.Data...))}
	case value.VAL_STRING:
		if lhs.Type != value.VAL_STRING {
			v.Panic("can only add string to string")
		}
		r := rhs.Payload.(string)
		l := lhs.Payload.(string)
		return value.StringVal(l + r)
	default:
		v.Panic("cannot use + on type")
	}
	// unreachable
	return value.Value{}
}

func (v *VM) equal(rhs value.Value, lhs value.Value) value.Value {
	switch rhs.Type {
	case value.VAL_INT:
		if lhs.Type == value.VAL_INT {
			if lhs.Payload.(int) == rhs.Payload.(int) {
				return value.VALUE_TRUE
			} else {
				return value.VALUE_FALSE
			}
		}
		fallthrough
	case value.VAL_DOUBLE:
		if toDouble(lhs) == toDouble(rhs) {
			return value.VALUE_TRUE
		} else {
			return value.VALUE_FALSE
		}
	case value.VAL_ARRAY:
		if lhs.Type != value.VAL_ARRAY {
			return value.VALUE_FALSE
		}
		r := rhs.Payload.(*value.Array)
		l := lhs.Payload.(*value.Array)
		if len(r.Data) != len(l.Data) {
			return value.VALUE_FALSE
		}
		for i, v := range r.Data {
			if v != l.Data[i] {
				return value.VALUE_FALSE
			}
		}
		return value.VALUE_TRUE
	case value.VAL_STRING:
		if lhs.Type != value.VAL_STRING {
			return value.VALUE_FALSE
		}
		r := rhs.Payload.(string)
		l := lhs.Payload.(string)
		if r == l {
			return value.VALUE_TRUE
		} else {
			return value.VALUE_FALSE
		}
	case value.VAL_TRUE:
		fallthrough
	case value.VAL_FALSE:
		if lhs.Type != rhs.Type {
			return value.VALUE_FALSE
		}
		return value.VALUE_TRUE
	default:
		v.Panic("cannot use == on type")
	}
	// unreachable
	return value.Value{}
}

func mul(rhs value.Value, lhs value.Value) value.Value {
	if lhs.Type == value.VAL_INT && rhs.Type == value.VAL_INT {
		return value.Value{Type: value.VAL_INT, Payload: lhs.Payload.(int) * rhs.Payload.(int)}
	} else {
		return value.Value{Type: value.VAL_DOUBLE, Payload: toDouble(lhs) * toDouble(rhs)}
	}
}

func div(rhs value.Value, lhs value.Value) value.Value {
	if lhs.Type == value.VAL_INT && rhs.Type == value.VAL_INT {
		return value.Value{Type: value.VAL_INT, Payload: lhs.Payload.(int) / rhs.Payload.(int)}
	} else {
		return value.Value{Type: value.VAL_DOUBLE, Payload: toDouble(lhs) / toDouble(rhs)}
	}
}

func (v *VM) buildArray() {
	count := v.advance()
	a := value.NewArray()
	if count > 0 {
		a.Data = append(a.Data, v.stack[len(v.stack)-int(count):len(v.stack)]...)
	}
	v.stack = v.stack[:len(v.stack)-int(count)]
	v.push(value.Value{Type: value.VAL_ARRAY, Payload: a})
}

func (v *VM) buildHash() {
	count := v.advance()
	h := value.NewHash()
	for ; count > 0; count-- {
		val := v.pop()
		k := v.pop()
		h.Insert(k, val)
	}
	v.push(value.Value{Type: value.VAL_HASH, Payload: h})
}

func (v *VM) captureUpValue(local *value.Value) value.UpValue {
	var prevUpValue *value.UpValue
	upValue := v.openUpValues
	for upValue != nil && uintptr(unsafe.Pointer(upValue.Location)) > uintptr(unsafe.Pointer(local)) {
		prevUpValue = upValue
		upValue = upValue.Next
	}

	if upValue != nil && uintptr(unsafe.Pointer(upValue.Location)) == uintptr(unsafe.Pointer(local)) {
		return *upValue
	}
	newUpValue := value.UpValue{Location: local}
	newUpValue.Next = upValue
	if prevUpValue == nil {
		v.openUpValues = &newUpValue
	} else {
		prevUpValue.Next = &newUpValue
	}
	return newUpValue
}

func (v *VM) closeUpValues(last *value.Value) {
	for v.openUpValues != nil && uintptr(unsafe.Pointer(v.openUpValues.Location)) >= uintptr(unsafe.Pointer(last)) {
		upValue := v.openUpValues
		upValue.Closed = *upValue.Location
		upValue.Location = &upValue.Closed
		v.openUpValues = upValue.Next
	}
}

func (v *VM) popFrame() {
	stackStart := v.currentFrame.stackStart
	if len(v.stack) > stackStart {
		v.closeUpValues(&v.stack[stackStart])
		v.stack = v.stack[:stackStart]
	}
	v.callFrame = v.callFrame[:len(v.callFrame)-1]
	if len(v.callFrame) > 0 {
		v.currentFrame = &v.callFrame[len(v.callFrame)-1]
	}
}

func (v *VM) greaterEqual() {
	rhs := v.pop()
	lhs := v.pop()
	if rhs.Type == value.VAL_INT && lhs.Type == value.VAL_INT {
		if lhs.Payload.(int) >= rhs.Payload.(int) {
			v.push(value.VALUE_TRUE)
		} else {
			v.push(value.VALUE_FALSE)
		}
	} else {
		v.Panic("no == defined for type")
	}
}

func (v *VM) lesserEqual() {
	rhs := v.pop()
	lhs := v.pop()
	if rhs.Type == value.VAL_INT && lhs.Type == value.VAL_INT {
		if lhs.Payload.(int) <= rhs.Payload.(int) {
			v.push(value.VALUE_TRUE)
		} else {
			v.push(value.VALUE_FALSE)
		}
	} else {
		v.Panic("no == defined for type")
	}
}

func (v *VM) run(returnFrame int) value.Value {
	for {
		c := v.advance()
		switch c {
		case opcode.OP_RETURN:
			val := v.pop()
			v.popFrame()
			if len(v.callFrame) == returnFrame {
				return val
			}
			v.push(val)
		default:
			table[c](v, returnFrame)
		}
	}
}

func (v *VM) Run() {
	v.run(0)
}
