package vm

import (
	"fmt"

	"codeberg.org/HanY/yoburg/lib/value"
)

func (v *VM) hashBuiltin(h *value.Hash, fn string, arity uint8) {
	switch fn {
	case "include?":
		if arity != 1 {
			panic(fmt.Sprintf("wrong number of args, expect 1 get %d", arity))
		}
		key := v.pop()
		_, ok := h.Data[key]
		if ok {
			v.stack[len(v.stack)-1] = value.VALUE_TRUE
		} else {
			v.stack[len(v.stack)-1] = value.VALUE_FALSE
		}
	case "to_s":
		if arity != 0 {
			panic("wrong number of arguments for 'to_s?' expect 0")
		}
		v.stack[len(v.stack)-1] = value.StringVal(h.ToString())
	default:
		panic(fmt.Sprintf("unsuported method %s on Hash", fn))
	}
}

func (v *VM) intBuiltin(i int, fn string, arity uint8) {
	switch fn {
	case "to_s":
		if arity != 0 {
			panic("wrong number of arguments for 'to_s?' expect 0")
		}
		v.stack[len(v.stack)-1] = value.StringVal(fmt.Sprint(i))
	default:
		panic(fmt.Sprintf("unsuported method %s on Int", fn))
	}
}

func (v *VM) doubleBuiltin(d float64, fn string, arity uint8) {
	switch fn {
	case "to_s":
		if arity != 0 {
			panic("wrong number of arguments for 'to_s?' expect 0")
		}
		v.stack[len(v.stack)-1] = value.StringVal(fmt.Sprint(d))
	default:
		panic(fmt.Sprintf("unsuported method %s on number", fn))
	}
}

func (v *VM) stringBuiltin(s string, fn string, arity uint8) {
	switch fn {
	case "to_s":
		if arity != 0 {
			panic("wrong number of arguments for 'to_s?' expect 0")
		}
		return
	default:
		panic(fmt.Sprintf("unsuported method %s on string", fn))
	}
}
