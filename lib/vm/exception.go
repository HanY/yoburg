package vm

import "codeberg.org/HanY/yoburg/lib/value"

func (v *VM) blockReturn(returnFrame int, returnType uint8) {
	val := v.pop()
	switch returnType {
	case value.EXCEPTION_BLOCK_RAISE:
		frameIndex := len(v.callFrame) - 1
		for frameIndex >= returnFrame {
			frame := &v.callFrame[frameIndex]
			if t := frame.ExceptionTable; t != nil {
				e, ok := t.FindEntry(v.currentFrame.pc, value.EXCEPTION_BLOCK_RAISE)
				if ok {
					v.callFrame = v.callFrame[:frameIndex+1]
					v.currentFrame = frame
					v.currentFrame.pc = e.End
					v.currentFrame.pc++ // advance pc to avoid catch rethrow
					v.addCallFrame(e.Function, v.currentFrame.stackStart)
					v.push(val)
					return
				}
			}
			frameIndex--
		}
	case value.EXCEPTION_BLOCK_RETURN:
		v.popFrame()
		for len(v.callFrame) > 0 {
			catch := false
			if t := v.currentFrame.ExceptionTable; t != nil {
				_, ok := t.FindEntry(v.currentFrame.pc, value.EXCEPTION_BLOCK_RETURN)
				catch = ok
			}
			if len(v.callFrame)-1 <= returnFrame {
				panic(val)
			}
			v.popFrame()
			if catch {
				v.push(val)
				return
			}
		}
	case value.EXCEPTION_BLOCK_BREAK:
		if t := v.currentFrame.ExceptionTable; t != nil {
			_, ok := t.FindEntry(v.currentFrame.pc, value.EXCEPTION_BLOCK_RETURN)
			if ok {
				v.popFrame()
				v.push(val)
			}
		}
	default:
		v.Panic("bug")
	}
	// uncatched exception
	v.Panic(value.ToString(val))
}
