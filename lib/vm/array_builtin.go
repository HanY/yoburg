package vm

import (
	"fmt"

	"codeberg.org/HanY/yoburg/lib/constants"
	"codeberg.org/HanY/yoburg/lib/value"
)

var arrayClass = value.NewClass("Array", constants.ARRAY_CLASS_ID)

func init() {
	arrayClass.Initializer = &value.NativeFunction{
		Name: "new",
		Func: func(caller value.Value, v []value.Value) value.Value {
			if len(v) == 0 {
				return value.Value{Type: value.VAL_ARRAY, Payload: &value.Array{Data: make([]value.Value, 0)}}
			} else if len(v) != 1 {
				panic("Array::new only accepts one arg")
			}
			if v[0].Type == value.VAL_INT {
				return value.Value{Type: value.VAL_ARRAY, Payload: value.NewArray()}
			} else if v[0].Type == value.VAL_ARRAY {
				return value.Value{Type: value.VAL_ARRAY, Payload: value.NewArrayFrom(v[0].Payload.(*value.Array).Data)}
			} else {
				panic("unknown type sent to Array::new")
			}
		},
		ParamList: value.ParamList{Arity: 1},
	}
}

func (v *VM) arrayBuiltin(a *value.Array, name string, arity uint8) {
	switch name {
	case "each":
		defer v.blockCatch(len(v.callFrame) - 1)
		b := v.pop()
		if b.Type != value.VAL_FUNC && b.Type != value.VAL_CLOSURE {
			panic("each expect block")
		}

		for _, val := range a.Data {
			v.InvokeBlock(b.Payload, []value.Value{val})
		}
	case "include?":
		if arity != 1 {
			panic("wrong number of arguments for 'include?' expect 1")
		}
		val := v.pop()
		for _, e := range a.Data {
			if e == val {
				v.stack[len(v.stack)-1] = value.VALUE_TRUE
				return
			}
		}
		v.stack[len(v.stack)-1] = value.VALUE_FALSE
	case "to_s":
		if arity != 0 {
			panic("wrong number of arguments for 'to_s?' expect 0")
		}
		v.stack[len(v.stack)-1] = value.StringVal(a.ToString())
	default:
		panic(fmt.Sprintf("undefined method %s for array", name))
	}
}

func stringArray(strArray []string) value.Value {
	ary := value.NewArray()
	for _, line := range strArray {
		ary.Data = append(ary.Data, value.StringVal(line))
	}
	return value.Value{Type: value.VAL_ARRAY, Payload: ary}
}
