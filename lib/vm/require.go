package vm

import (
	"fmt"
	"os"
	"path"

	"codeberg.org/HanY/yoburg/lib/common_error"
	"codeberg.org/HanY/yoburg/lib/compiler"
	"codeberg.org/HanY/yoburg/lib/scanner"
	"codeberg.org/HanY/yoburg/lib/value"
)

func (v *VM) requireRelative(file string) {
	info, ok := v.debugInfos[v.currentFrame.Function]
	if !ok {
		panic("can't find debuginfo")
	}
	dir := path.Dir(info.Filename)
	filepath := path.Join(dir, file)
	fi, err := os.Open(filepath)
	if err != nil {
		panic(err)
	}
	s := scanner.NewScanner(fi)
	c := compiler.NewCompilerWithDefault(s,
		file,
		&v.stringConstMap,
		&v.internedStrings,
		&v.debugInfos)
	defer func() {
		err := recover()
		if err == nil {
			return
		}
		syntaxErr, ok := err.(common_error.SyntaxError)
		if !ok {
			panic(err)
		}
		se := SyntaxErrorClass.NewInstance()
		se.IVar["message"] = value.StringVal(fmt.Sprintf("in %s:%d: syntax error, %s",
			syntaxErr.Filename,
			syntaxErr.Line,
			syntaxErr.Message))
		v.push(value.Value{Type: value.VAL_INSTANCE, Payload: se})
		opRaise(v, 0 /* unused */)
	}()
	ret := c.Compile()
	v.push(v.stack[0])
	v.addCallFrame(ret.Function, len(v.stack)-1)
	v.run(len(v.callFrame) - 1)
	v.push(value.VALUE_TRUE)
}
