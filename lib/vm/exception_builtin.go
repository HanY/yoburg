package vm

import (
	"codeberg.org/HanY/yoburg/lib/constants"
	"codeberg.org/HanY/yoburg/lib/value"
)

var ExceptionClass = value.NewClass("Exception", constants.EXCEPTION_CLASS_ID)
var RuntimeErrorClass = value.NewClass("RuntimeError", constants.RUNTIME_ERROR_CLASS_ID)
var SyntaxErrorClass = value.NewClass("SyntaxError", constants.SYNTAX_ERROR_CLASS_ID)
var TypeErrorClass = value.NewClass("TypeError", constants.TYPE_ERROR_CLASS_ID)

func init() {
	ExceptionClass.Methods["backtrace"] = &value.NativeFunction{
		Func: func(caller value.Value, v []value.Value) value.Value {
			if len(v) != 0 {
				panic("wrong number of arguements")
			}
			i, ok := caller.Payload.(*value.Instance)
			if !ok {
				panic("bug")
			}
			trace, ok := i.IVar["backtrace"]
			if !ok {
				return value.VALUE_NIL
			}
			return trace
		},
		Name: "backtrace",
	}
	ExceptionClass.Methods["set_backtrace"] = &value.NativeFunction{
		Func: func(caller value.Value, v []value.Value) value.Value {
			if len(v) != 1 {
				panic("wrong number of arguements")
			}

			trace, ok := v[0].Payload.(*value.Array)
			if !ok {
				panic("backtrace must be array of string of specified format")
			}
			i, ok := caller.Payload.(*value.Instance)
			if !ok {
				panic("bug")
			}

			for _, val := range trace.Data {
				// TODO
				_, ok := val.Payload.(string)
				if !ok {
					panic("backtrace must be array of string of specified format")
				}

			}

			i.IVar["backtrace"] = v[0]
			if !ok {
				return value.VALUE_NIL
			}
			return v[0]
		},
		ParamList: value.ParamList{Arity: 1},
		Name:      "set_backtrace",
	}
	ExceptionClass.ChildClasses = append(ExceptionClass.ChildClasses,
		RuntimeErrorClass,
		SyntaxErrorClass,
		TypeErrorClass)
	SyntaxErrorClass.SuperClass = ExceptionClass
	RuntimeErrorClass.SuperClass = ExceptionClass
	TypeErrorClass.SuperClass = ExceptionClass
}
