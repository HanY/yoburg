package vm

import (
	"fmt"
	"math"

	"codeberg.org/HanY/yoburg/lib/opcode"
	"codeberg.org/HanY/yoburg/lib/value"
)

type TableFunc func(*VM, int)

type VMTable [opcode.OP_NOP + 1]TableFunc

var table VMTable = VMTable{}

func init() {
	table[opcode.OP_LOAD_CONST] = opLoadConst
	table[opcode.OP_LOAD_CONST_U16] = opLoadConstU16
	table[opcode.OP_LOAD_STRING_8] = opLoadString8
	table[opcode.OP_LOAD_STRING_16] = opLoadString16
	table[opcode.OP_LOAD_STRING_32] = opLoadString32
	table[opcode.OP_INT_8] = opInt8
	table[opcode.OP_INT_16] = opInt16
	table[opcode.OP_INT_32] = opInt32
	table[opcode.OP_INT_64] = opInt64
	table[opcode.OP_DOUBLE] = opDouble
	table[opcode.OP_SUB] = opSub
	table[opcode.OP_ADD] = opAdd
	table[opcode.OP_MUL] = opMul
	table[opcode.OP_DIV] = opDiv
	table[opcode.OP_SET_GLOBAL] = opSetGlobal
	table[opcode.OP_GET_GLOBAL] = opGetGlobal
	table[opcode.OP_SET_LOCAL] = opSetLocal
	table[opcode.OP_GET_LOCAL] = opGetLocal
	table[opcode.OP_POP] = opPop
	table[opcode.OP_FALSE] = opFalse
	table[opcode.OP_TRUE] = opTrue
	table[opcode.OP_NIL] = opNil
	table[opcode.OP_JUMP] = opJump
	table[opcode.OP_JUMP_IF_FALSE] = opJumpIfFalse
	table[opcode.OP_JUMP_IF_TRUE] = opJumpIfTrue
	table[opcode.OP_BRANCH_IF_FALSE] = opBranchIfFalse
	table[opcode.OP_LOOP] = opLoop
	table[opcode.OP_NOT] = opNot
	table[opcode.OP_NEGATE] = opNegate
	table[opcode.OP_EQUAL] = opEqual
	table[opcode.OP_GREATER_EQUAL] = opGreaterEqual
	table[opcode.OP_LESSER_EQUAL] = opLesserEqual
	table[opcode.OP_METHOD] = opMethod
	table[opcode.OP_GLOBAL_METHOD] = opGloablMethod
	table[opcode.OP_STATIC_METHOD] = opStaticMethod
	table[opcode.OP_CALL] = opCall
	table[opcode.OP_DOT_CALL] = opDotCall
	table[opcode.OP_OPTIONAL_CALL] = opOptionalCall
	table[opcode.OP_CALL_BLOCK] = opCallBlock
	table[opcode.OP_RETURN_BLOCK] = opReturnBlock
	table[opcode.OP_RETURN_RESCUE] = opReturnRescue
	table[opcode.OP_CLASS] = opClass
	table[opcode.OP_LOAD_CLASS_CONST] = opLoadClassConst
	table[opcode.OP_SET_CLASS_CONST] = opSetClassConst
	table[opcode.OP_GET_IVAR] = opGetIVar
	table[opcode.OP_SET_IVAR] = opSetIVar
	table[opcode.OP_BUILD_ARRAY] = opBuildArray
	table[opcode.OP_SUBSCRIPT] = opSubscript
	table[opcode.OP_SUBSCRIPT_SET] = opSubscriptSet
	table[opcode.OP_BUILD_HASH] = opBuildHash
	table[opcode.OP_CLOSURE] = opClosure
	table[opcode.OP_GET_UPVALUE] = opGetUpValue
	table[opcode.OP_SET_UPVALUE] = opSetUpValue
	table[opcode.OP_DUP] = opDup
	table[opcode.OP_INHERIT] = opInherit
	table[opcode.OP_CHECK_CLASS] = opCheckClass
	table[opcode.OP_RAISE] = opRaise
	table[opcode.OP_SIMPLE_RAISE] = opSimpleRaise
	table[opcode.OP_REQUIRE_RELATIVE] = opRequrieRelative
	table[opcode.OP_NOP] = opNop
}

func opNop(v *VM, returnFrame int) {

}

func opLoadConst(v *VM, returnFrame int) {
	v.push(v.currentFrame.Constants[v.advance()])
}

func opLoadConstU16(v *VM, returnFrame int) {
	v.push(v.currentFrame.Constants[v.readUInt16()])
}

func opLoadString8(v *VM, returnFrame int) {
	v.push(value.StringVal(v.internedStrings[v.advance()]))
}

func opLoadString16(v *VM, returnFrame int) {
	v.push(value.StringVal(v.internedStrings[v.readUInt16()]))
}
func opLoadString32(v *VM, returnFrame int) {
	v.push(value.StringVal(v.internedStrings[v.readUInt32()]))
}

func opInt8(v *VM, returnFrame int) {
	v.push(value.IntVal(int(v.advance())))
}

func opInt16(v *VM, returnFrame int) {
	v.push(value.IntVal(int(v.readUInt16())))
}

func opInt32(v *VM, returnFrame int) {
	v.push(value.IntVal(int(v.readUInt32())))
}

func opInt64(v *VM, returnFrame int) {
	v.push(value.IntVal(int(v.readUInt64())))
}

func opDouble(v *VM, returnFrame int) {
	v.push(value.DoubleVal(math.Float64frombits(v.readUInt64())))
}

func opAdd(v *VM, returnFrame int) {
	lhs := v.pop()
	v.stack[len(v.stack)-1] = v.add(lhs, v.stack[len(v.stack)-1])
}

func opSub(v *VM, returnFrame int) {
	lhs := v.pop()
	v.stack[len(v.stack)-1] = sub(lhs, v.stack[len(v.stack)-1])
}

func opMul(v *VM, returnFrame int) {
	lhs := v.pop()
	v.stack[len(v.stack)-1] = mul(lhs, v.stack[len(v.stack)-1])
}

func opDiv(v *VM, returnFrame int) {
	lhs := v.pop()
	v.stack[len(v.stack)-1] = div(lhs, v.stack[len(v.stack)-1])
}

func opSetGlobal(v *VM, returnFrame int) {
	name := v.internedStrings[v.readUInt16()]
	v.globals[name] = v.stackTop()
}

func opGetGlobal(v *VM, returnFrame int) {
	name := v.internedStrings[v.readUInt16()]
	if val, ok := v.globals[name]; ok {
		v.push(val)
	} else {
		v.push(value.VALUE_NIL)
	}
}

func opSetLocal(v *VM, returnFrame int) {
	v.stack[v.currentFrame.stackStart+int(v.advance())] = v.stackTop()
}

func opGetLocal(v *VM, returnFrame int) {
	v.push(v.stack[v.currentFrame.stackStart+int(v.advance())])
}

func opPop(v *VM, returnFrame int) {
	v.pop()
}
func opNil(v *VM, returnFrame int) {
	v.push(value.VALUE_NIL)
}

func opFalse(v *VM, returnFrame int) {
	v.push(value.VALUE_FALSE)
}

func opTrue(v *VM, returnFrame int) {
	v.push(value.VALUE_TRUE)
}

func opJump(v *VM, returnFrame int) {
	v.currentFrame.pc += int(v.readUInt16())
}

func opJumpIfFalse(v *VM, returnFrame int) {
	offset := v.readUInt16()
	if value.IsFalsey(v.stackTop()) {
		v.currentFrame.pc += int(offset)
	}
}

func opBranchIfFalse(v *VM, returnFrame int) {
	offset := v.readUInt16()
	if value.IsFalsey(v.stackTop()) {
		v.currentFrame.pc += int(offset)
	}
	v.pop()
}

func opJumpIfTrue(v *VM, returnFrame int) {
	offset := v.readUInt16()
	if !value.IsFalsey(v.stackTop()) {
		v.currentFrame.pc += int(offset)
	}
}

func opLoop(v *VM, returnFrame int) {
	offset := v.readUInt16()
	v.currentFrame.pc -= int(offset)
}

func opNot(v *VM, returnFrame int) {
	if value.IsFalsey(v.stackTop()) {
		v.stack[len(v.stack)-1] = value.VALUE_TRUE
	} else {
		v.stack[len(v.stack)-1] = value.VALUE_FALSE
	}
}

func opNegate(v *VM, returnFrame int) {
	switch v.stackTop().Type {
	case value.VAL_DOUBLE:
		v.stack[len(v.stack)-1].Payload = -v.stackTop().Payload.(float64)
	case value.VAL_INT:
		v.stack[len(v.stack)-1].Payload = -v.stackTop().Payload.(int)
	default:
		v.Panic("Unknown value for '-'")
	}
}

func opEqual(v *VM, returnFrame int) {
	rhs := v.pop()
	lhs := v.stack[len(v.stack)-1]
	v.stack[len(v.stack)-1] = v.equal(rhs, lhs)
}

func opGreaterEqual(v *VM, returnFrame int) {
	v.greaterEqual()
}

func opLesserEqual(v *VM, returnFrame int) {
	v.lesserEqual()
}

func opMethod(v *VM, returnFrame int) {
	v.defineMethod(false)
}

func opGloablMethod(v *VM, returnFrame int) {
	v.defineMethod(true)
}

func opStaticMethod(v *VM, returnFrame int) {
	v.defineStaticMethod()
}

func opCall(v *VM, returnFrame int) {
	v.opCall(true, false)
}

func opDotCall(v *VM, returnFrame int) {
	v.opCall(false, false)
}

func opOptionalCall(v *VM, returnFrame int) {
	v.opCall(false, true)
}

func opCallBlock(v *VM, returnFrame int) {
	arity := v.advance()
	fn := v.pop().Payload
	switch f := fn.(type) {
	case *value.Function:
		v.callFuncSimple(arity, f)
	case *value.Closure:
		v.callClosure(arity, f)
	default:
		v.Panic("bug")
	}
}

func opReturnBlock(v *VM, returnFrame int) {
	v.blockReturn(returnFrame, value.EXCEPTION_BLOCK_RETURN)
}

func opReturnRescue(v *VM, returnFrame int) {
	v.callFrame = v.callFrame[:len(v.callFrame)-1]
	v.currentFrame = &v.callFrame[len(v.callFrame)-1]
}

func opClass(v *VM, returnFrame int) {
	className := v.pop().Payload.(string)
	fn := v.stackTop().Payload.(*value.Function)
	v.stack[len(v.stack)-1] = v.createClass(className)
	v.addCallFrame(fn, len(v.stack)-1)
}

func opLoadClassConst(v *VM, returnFrame int) {
	name := v.pop().Payload.(string)
	v.stack[len(v.stack)-1] = v.findClassConstant(name)
}

func opSetClassConst(v *VM, returnFrame int) {
	name := v.pop().Payload.(string)
	val := v.stackTop()
	var c *value.Class
	switch caller := v.caller().Payload.(type) {
	case *value.Class:
		c = caller
	case *value.Instance:
		if caller == v.global() {
			c = caller.Class
		} else {
			v.Panic("can't access constant from instance")
		}
	}
	if _, ok := c.Constants[name]; ok {
		fmt.Printf("Constant %s has already been defined\n", name)
	}
	c.Constants[name] = val
}

func opGetIVar(v *VM, returnFrame int) {
	name := v.stack[len(v.stack)-1].Payload.(string)
	i := v.caller().Payload.(*value.Instance)
	if val, ok := i.IVar[name]; ok {
		v.stack[len(v.stack)-1] = val
	} else {
		v.stack[len(v.stack)-1] = value.VALUE_NIL
	}
}

func opSetIVar(v *VM, returnFrame int) {
	name := v.pop().Payload.(string)
	i := v.caller().Payload.(*value.Instance)
	i.IVar[name] = v.stackTop()
}

func opBuildArray(v *VM, returnFrame int) {
	v.buildArray()
}

func opSubscript(v *VM, returnFrame int) {
	v.opSubscript()
}

func opSubscriptSet(v *VM, returnFrame int) {
	v.opSubscriptSet()
}

func opBuildHash(v *VM, returnFrame int) {
	v.buildHash()
}

func opClosure(v *VM, returnFrame int) {
	f := v.currentFrame.Function.Constants[v.readUInt16()].Payload.(*value.Function)
	c := value.NewClosure(f)
	for i := uint8(0); i < f.UpvalueCount; i++ {
		isLocal := v.advance()
		index := v.advance()
		if isLocal == 1 {
			c.UpValues = append(c.UpValues, v.captureUpValue(&v.stack[v.currentFrame.stackStart+int(index)]))
		} else {
			c.UpValues = append(c.UpValues, v.currentFrame.UpValues[index])
		}
	}
	v.push(value.Value{Type: value.VAL_CLOSURE,
		Payload: c})
}

func opGetUpValue(v *VM, returnFrame int) {
	index := v.advance()
	v.push(*v.currentFrame.UpValues[index].Location)
}

func opSetUpValue(v *VM, returnFrame int) {
	index := v.advance()
	*v.currentFrame.UpValues[index].Location = v.stackTop()
}

func opDup(v *VM, returnFrame int) {
	v.push(v.stack[len(v.stack)-1])
}

func opInherit(v *VM, returnFrame int) {
	super := v.pop().Payload.(*value.Class)
	c := v.stackTop().Payload.(*value.Class)
	c.SuperClass = super
	super.ChildClasses = append(super.ChildClasses, c)
	if c.IsIncest(super) {
		v.Panic(fmt.Sprintf("%s is super class of %s", c.Name, super.Name))
	}
}

func opCheckClass(v *VM, returnFrame int) {
	class := v.pop()
	if class.Type != value.VAL_CLASS {
		v.Panic(fmt.Sprintf("%s is not class", value.ToString(class)))
	}
	if v.stackTop().Payload.(*value.Instance).IsInstanceOf(class.Payload.(*value.Class)) {
		v.stack[len(v.stack)-1] = value.VALUE_TRUE
	} else {
		v.stack[len(v.stack)-1] = value.VALUE_FALSE
	}
}

func (v *VM) buildBacktrace() []string {
	ary := make([]string, len(v.callFrame))
	i := 0
	frameIndex := len(v.callFrame) - 1
	frame := &v.callFrame[frameIndex]
	for frameIndex > 0 {
		info, ok := v.debugInfos[frame.Function]
		if ok {
			line := info.FindLine(v.currentFrame.pc - 1)
			ary[i] = fmt.Sprintf("from %s:%d: in `%s`\n", info.Filename, line, frame.Name)
			i++
		} else {
			break
		}
		frameIndex--
		frame = &v.callFrame[frameIndex]
	}
	return ary
}

func opRaise(v *VM, returnFrame int) {
	v.blockReturn(returnFrame, value.EXCEPTION_BLOCK_RAISE)
}

func opSimpleRaise(v *VM, returnFrame int) {
	e := RuntimeErrorClass.NewInstance()
	e.IVar["backtrace"] = stringArray(v.buildBacktrace())
	v.push(value.Value{Type: value.VAL_INSTANCE, Payload: e})
	v.blockReturn(returnFrame, value.EXCEPTION_BLOCK_RAISE)
}

func opRequrieRelative(v *VM, returnFrame int) {
	path, ok := v.pop().Payload.(string)
	if !ok {
		e := TypeErrorClass.NewInstance()
		e.IVar["backtrace"] = stringArray(v.buildBacktrace())
		e.IVar["message"] = value.StringVal("require only accepts string")
		opRaise(v, returnFrame)
		return
	}
	v.requireRelative(path)
}
