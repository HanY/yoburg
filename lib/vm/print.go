package vm

import (
	"fmt"
	"strings"

	"codeberg.org/HanY/yoburg/lib/opcode"
	"codeberg.org/HanY/yoburg/lib/value"
)

func PrintExceptionTable(t value.ExceptionTable, index int, internedStrings []string) {
	b := strings.Builder{}

	for i := 0; i < index; i++ {
		b.WriteByte(' ')
	}
	indentStr := b.String()

	for i, entry := range t {
		var exceptionType string
		switch entry.Type {
		case value.EXCEPTION_BLOCK_BREAK:
			exceptionType = "break"
		case value.EXCEPTION_BLOCK_NEXT:
			exceptionType = "next"
		case value.EXCEPTION_BLOCK_RAISE:
			exceptionType = "raise"
		case value.EXCEPTION_BLOCK_RETURN:
			exceptionType = "return"
		}
		fmt.Printf("%s%d: <%s s:%d e:%d>\n", indentStr, i, exceptionType, entry.Start, entry.End)
		if entry.Type == value.EXCEPTION_BLOCK_RAISE {
			PrintFunc(entry.Function, index+2, internedStrings)
		}
	}
}

func PrintFunc(f *value.Function, index int, internedStrings []string) {
	b := strings.Builder{}
	for i := 0; i < index; i++ {
		b.WriteByte(' ')
	}
	indentStr := b.String()

	var block string
	var keywords string
	if f.Block {
		block = " block:true"
	}
	if f.Keywords != nil {
		b := strings.Builder{}
		b.WriteByte(' ')
		for _, k := range f.Keywords {
			b.WriteString(fmt.Sprintf(":%s ", k.Name))
		}
		keywords = b.String()
	}
	fmt.Printf("%s<function #%s local:%d%s%s>\n", indentStr, f.Name, f.LocalCount, block, keywords)
	if f.ExceptionTable != nil {
		fmt.Printf("%s    Exception Table:\n", indentStr)
		PrintExceptionTable(f.ExceptionTable, 6+index, internedStrings)
	}
	opcode.PrintChunk(&f.Chunk, internedStrings, 4+index)
}

func PrintChunk(c value.Chunk, internedStrings []string) {
	fmt.Print("Constants:\n")
	for i, val := range c.Constants {
		fmt.Printf("  %d: ", i)
		if val.Type == value.VAL_FUNC {
			fn := val.Payload.(*value.Function)
			PrintFunc(fn, 0, internedStrings)
		} else {
			value.PrintValue(val)
		}
	}
	fmt.Print("OP Codes:\n")
	opcode.PrintChunk(&c, internedStrings, 2)
}
