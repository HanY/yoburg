package value

type ExceptionTableEntry struct {
	Start int
	End   int
	Type  uint8
	*Function
}

const (
	EXCEPTION_BLOCK_RETURN uint8 = iota
	EXCEPTION_BLOCK_BREAK
	EXCEPTION_BLOCK_NEXT
	EXCEPTION_BLOCK_RAISE
)

type ExceptionTable []ExceptionTableEntry

func (t ExceptionTable) FindEntry(pc int, entryType uint8) (ExceptionTableEntry, bool) {
	for _, e := range t {
		if e.Start <= pc && e.End >= pc && e.Type == entryType {
			return e, true
		}
	}
	return ExceptionTableEntry{}, false
}
