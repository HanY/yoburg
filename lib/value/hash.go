package value

import (
	"fmt"
	"strings"
)

type Hash struct {
	Data map[Value]Value
}

func NewHash() *Hash {
	return &Hash{Data: make(map[Value]Value)}
}

func (h *Hash) Insert(k Value, v Value) {
	h.Data[k] = v
}

func (h *Hash) Get(k Value) Value {
	v, ok := h.Data[k]
	if !ok {
		return VALUE_NIL
	}
	return v
}

func (h *Hash) ToString() string {
	b := strings.Builder{}
	b.WriteByte('{')
	for k, v := range h.Data {
		b.WriteString(fmt.Sprintf("%s => %s, ", ToString(k), ToString(v)))
	}
	b.WriteByte('}')
	return b.String()
}
