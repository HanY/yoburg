package value

type Class struct {
	Name         string
	ID           int
	Methods      map[string]interface{}
	Constants    map[string]Value
	Initializer  interface{}
	Enclosing    *Class
	SuperClass   *Class
	ChildClasses []*Class
}

type Instance struct {
	*Class
	IVar map[string]Value
}

func NewClass(name string, id int) *Class {
	return &Class{Name: name,
		ID:        id,
		Methods:   make(map[string]interface{}),
		Constants: map[string]Value{}}
}

func (c *Class) NewInstance() *Instance {
	return &Instance{Class: c, IVar: make(map[string]Value)}
}

func (c *Class) SetId(id int) {
	c.ID = id
	for _, cc := range c.ChildClasses {
		cc.SetId(id)
	}
}

func (i *Instance) IsInstanceOf(c *Class) bool {
	ancestor := i.Class
	for ancestor.ID != c.ID {
		ancestor = ancestor.SuperClass
		if ancestor == nil {
			return false
		}
	}
	return true
}

func (c *Class) IsIncest(super *Class) bool {
	slow := c
	quick := super
	for slow != nil && quick != nil {
		if slow == quick {
			return true
		}
		slow = slow.SuperClass
		quick = quick.SuperClass
		if quick != nil {
			quick = quick.SuperClass
		}
	}
	return false
}
