package value

type OpcodeToLine struct {
	OpcodeStart int
	Line        int
}

type DebugInfo struct {
	Filename string
	Lines    []OpcodeToLine
}

type Chunk struct {
	Code      []uint8
	Constants []Value
	CallInfos []*CallInfo
}

func (d *DebugInfo) InsertDebugInfo(opcodeIndex int, line int) {
	if d.Lines == nil {
		d.Lines = []OpcodeToLine{{opcodeIndex, line}}
		return
	}

	if d.Lines[len(d.Lines)-1].Line == line {
		return
	}
	d.Lines = append(d.Lines, OpcodeToLine{OpcodeStart: opcodeIndex, Line: line})
}

func (d *DebugInfo) FindLine(opcodeIndex int) int {
	i := 0
	length := len(d.Lines)
	if length == 0 {
		panic("bug")
	}
	for i < length {
		if d.Lines[i].OpcodeStart <= opcodeIndex && (i+1 >= length || d.Lines[i+1].OpcodeStart > opcodeIndex) {
			return d.Lines[i].Line
		}
		i++
	}
	return d.Lines[len(d.Lines)-1].Line
}
