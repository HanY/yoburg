package value

import (
	"unsafe"
)

type Keyword struct {
	Name       string
	DefaultVal Value
}

type ParamList struct {
	Arity    uint8
	Block    bool
	VarArg   bool
	Keywords []Keyword
}

type Function struct {
	Chunk
	ParamList
	Name string
	ExceptionTable
	LocalCount   uint8
	UpvalueCount uint8
}

type NativeFunction struct {
	Func func(Value, []Value) Value
	ParamList
	Name string
}

func NewFunction(name string) *Function {
	return &Function{Name: name}
}

func (p *ParamList) IndexKeyword(n string) int {
	for i, k := range p.Keywords {
		if k.Name == n {
			return i
		}
	}
	return -1
}

func (p *ParamList) NonSimpleArgs() bool {
	return p.Block || p.Keywords != nil || p.VarArg
}

func (f *Function) AddCallInfo(i CallInfo) {
	f.CallInfos = append(f.CallInfos, &i)
	binary := (*[unsafe.Sizeof(i)]uint8)(unsafe.Pointer(&i))
	for _, b := range binary {
		f.Code = append(f.Code, b)
	}
}

func (f *Function) AddExceptionRescueEntry(start int, end int, fn *Function) {
	f.ExceptionTable = append(f.ExceptionTable,
		ExceptionTableEntry{Start: start,
			End:      end,
			Type:     EXCEPTION_BLOCK_RAISE,
			Function: fn})
}

func (f *Function) AddExceptionBreakEntry(start int, end int) {
	f.ExceptionTable = append(f.ExceptionTable,
		ExceptionTableEntry{Start: start,
			End:  end,
			Type: EXCEPTION_BLOCK_BREAK})
}

func (f *Function) AddExceptionReturnEntry(start int, end int) {
	f.ExceptionTable = append(f.ExceptionTable,
		ExceptionTableEntry{Start: start,
			End:  end,
			Type: EXCEPTION_BLOCK_RETURN})
}

func (f *Function) AddExceptionNextEntry(start int, end int) {
	f.ExceptionTable = append(f.ExceptionTable,
		ExceptionTableEntry{Start: start,
			End:  end,
			Type: EXCEPTION_BLOCK_NEXT})
}
