package value

type UpValue struct {
	Location *Value
	Next     *UpValue
	Closed   Value
}

type Closure struct {
	*Function
	UpValues []UpValue
}

func NewClosure(f *Function) *Closure {
	return &Closure{Function: f}
}
