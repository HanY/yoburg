package value

import (
	"strings"
)

type Array struct {
	Data []Value
}

func NewArray() *Array {
	a := &Array{Data: make([]Value, 5)}
	return a
}

func NewArrayFrom(v []Value) *Array {
	a := &Array{Data: make([]Value, len(v))}

	copy(a.Data, v)
	return a
}

func (a *Array) ToString() string {
	b := strings.Builder{}
	b.WriteByte('[')
	for _, v := range a.Data {
		b.WriteString(ToString(v))
		b.Write([]byte(", "))
	}
	b.WriteByte(']')
	return b.String()
}
