package value

type InlineCache struct {
	Function interface{} // Function or NativeFunction
	ClassID  int
	Global   bool
}

type CallInfo struct {
	Keys  []string
	Block bool
	Name  string
	Arity uint8
	// inline cache
	Cache InlineCache
}

func (i *CallInfo) ArgCount() int {
	ret := int(i.Arity) + len(i.Keys)
	if i.Block {
		ret++
	}
	return ret
}
