package value

import (
	"fmt"
	"strings"
)

const (
	VAL_NIL uint8 = iota
	VAL_TRUE
	VAL_FALSE
	VAL_OBJ
	VAL_INT
	VAL_DOUBLE
	VAL_FUNC
	VAL_CLASS
	VAL_INSTANCE
	VAL_STRING
	VAL_NATIVE
	VAL_ARRAY
	VAL_HASH
	VAL_CALLINFO
	VAL_UPVALUE
	VAL_CLOSURE
)

type Value struct {
	Type    uint8
	Payload interface{}
}

var VALUE_NIL = Value{Type: VAL_NIL}
var VALUE_FALSE = Value{Type: VAL_FALSE}
var VALUE_TRUE = Value{Type: VAL_TRUE}

func StringVal(s string) Value {
	return Value{Type: VAL_STRING, Payload: s}
}

func IntVal(i int) Value {
	return Value{Type: VAL_INT, Payload: i}
}

func DoubleVal(d float64) Value {
	return Value{Type: VAL_DOUBLE, Payload: d}
}

func IsFalsey(v Value) bool {
	if v == VALUE_NIL || v == VALUE_FALSE {
		return true
	}
	return false
}

func ToString(v Value) string {
	var str string
	switch v.Type {
	case VAL_NIL:
		str = "nil"
	case VAL_TRUE:
		str = "true"
	case VAL_FALSE:
		str = "false"
	case VAL_OBJ:
		str = "OBJ"
	case VAL_INT:
		str = fmt.Sprintf("%d", v.Payload.(int))
	case VAL_DOUBLE:
		str = fmt.Sprintf("%f", v.Payload.(float64))
	case VAL_FUNC:
		str = fmt.Sprintf("<function %s>", v.Payload.(*Function).Name)
	case VAL_CLASS:
		str = fmt.Sprintf("<class %s>", v.Payload.(*Class).Name)
	case VAL_INSTANCE:
		str = fmt.Sprintf("<instance of %s>", v.Payload.(*Instance).Name)
	case VAL_NATIVE:
		str = fmt.Sprintf("<native function %s>", v.Payload.(*NativeFunction).Name)
	case VAL_STRING:
		str = fmt.Sprintf("'%s'", v.Payload.(string))
	case VAL_ARRAY:
		str = v.Payload.(*Array).ToString()
	case VAL_HASH:
		str = v.Payload.(*Hash).ToString()
	case VAL_CALLINFO:
		str = fmt.Sprintf("<Call Info #Keys: [%s]>", strings.Join(v.Payload.(*CallInfo).Keys, " "))
	case VAL_UPVALUE:
		str = "<upvalue>"
	case VAL_CLOSURE:
		str = "<closure>"
	}
	return str
}

func PrintValue(v Value) {
	fmt.Printf("%s\n", ToString(v))
}
