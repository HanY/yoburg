package opcode

import (
	"fmt"
	"math"
	"strings"
	"unsafe"

	"codeberg.org/HanY/yoburg/lib/util"
	"codeberg.org/HanY/yoburg/lib/value"
)

const (
	OP_RETURN        uint8 = iota
	OP_RETURN_BLOCK        // return from block, return to caller's caller
	OP_RETURN_RESCUE       // pop the frame, but don't reset the stack
	OP_DUP                 // duplicate value on the top of the stack (shallow)
	OP_NEGATE
	OP_ADD
	OP_SUB
	OP_MUL
	OP_DIV
	OP_PUSH
	OP_POP
	OP_NOT
	OP_CALL_BLOCK // block on the top, params below
	OP_CALL       // 8 bit arity. The stack looks like this
	/*
		STACK_TOP: method_name
				   arg_n
				   ...
				   arg_0
				   caller
	*/
	OP_DOT_CALL      // same as above, but without global method fallback
	OP_OPTIONAL_CALL // dot call, but if caller is nil, return nil
	OP_METHOD        // the method name is on stack top. Class is the caller
	OP_STATIC_METHOD // same as above, but define to static method
	OP_GLOBAL_METHOD // same as OP_METHOD
	OP_CLASS         /*
		STACK_TOP: class
				   caller
	*/
	OP_LOAD_CONST
	OP_LOAD_CONST_U16
	OP_INT_8
	OP_INT_16
	OP_INT_32
	OP_INT_64
	OP_DOUBLE // next 64 bits as value
	OP_LOAD_STRING_8
	OP_LOAD_STRING_16
	OP_LOAD_STRING_32
	OP_LOAD_CLASS_CONST // Runtime class constants
	OP_SET_CLASS_CONST
	OP_SET_LOCAL
	OP_GET_LOCAL
	OP_GET_UPVALUE
	OP_SET_UPVALUE
	OP_CLOSURE
	OP_SET_GLOBAL
	OP_GET_GLOBAL
	OP_GET_IVAR // STACK_TOP: name
	OP_SET_IVAR
	// STACK_TOP: name
	//			  value
	OP_JUMP_IF_FALSE
	OP_JUMP_IF_TRUE
	OP_BRANCH_IF_FALSE
	OP_JUMP
	OP_LOOP
	OP_NIL
	OP_FALSE
	OP_TRUE
	OP_EQUAL
	OP_GREATER_EQUAL
	OP_LESSER_EQUAL
	OP_BUILD_ARRAY   // followed by 8 bit element count
	OP_SUBSCRIPT     // key on top of the stack, followed by object
	OP_SUBSCRIPT_SET // val on top of the stack, followed by key and object
	OP_BUILD_HASH    // followed by 8 bit pair count
	OP_RAISE         // raise with arg
	OP_SIMPLE_RAISE  // raise without arg
	OP_INHERIT
	OP_CHECK_CLASS
	OP_REQUIRE_RELATIVE
	OP_NOP // keep this at end
)

func PrintChunk(chunk *value.Chunk, internedString []string, indent int) {
	b := strings.Builder{}
	for i := 0; i < indent; i++ {
		b.WriteByte(' ')
	}
	spaces := b.String()
	codes := chunk.Code
	for i := 0; i < len(codes); i++ {
		fmt.Printf("%s%d: ", spaces, i)
		code := codes[i]
		switch code {
		case OP_LOAD_CONST_U16:
			i = i + 2
			fmt.Printf("<OP_LOAD_CONST_U16 %d>\n", util.FromBytes2(codes[i-1:i+1]))
		case OP_LOAD_CONST:
			i++
			fmt.Printf("<OP_LOAD_CONST %d>\n", codes[i])
		case OP_INT_8:
			i++
			fmt.Printf("<OP_INT_8 %d>\n", int(codes[i]))
		case OP_INT_16:
			i += 2
			fmt.Printf("<OP_INT_16 %d>\n", int(util.FromBytes2(codes[i-1:i+1])))
		case OP_INT_32:
			i += 4
			fmt.Printf("<OP_INT_32 %d>\n", int(util.FromBytes4(codes[i-3:i+1])))
		case OP_INT_64:
			i += 8
			fmt.Printf("<OP_INT_64 %d>\n", int(util.FromBytes8(codes[i-7:i+1])))
		case OP_DOUBLE:
			i += 8
			fmt.Printf("<OP_DOUBLE %f>\n", math.Float64frombits(util.FromBytes8(codes[i-7:i+1])))
		case OP_LOAD_STRING_8:
			i++
			fmt.Printf("<OP_LOAD_STRING_8 %s>\n", internedString[codes[i]])
		case OP_LOAD_STRING_16:
			i += 2
			fmt.Printf("<OP_LOAD_STRING_16 %s>\n", internedString[util.FromBytes2(codes[i-1:i+1])])
		case OP_LOAD_STRING_32:
			i += 4
			fmt.Printf("<OP_LOAD_STRING_32 %s>\n", internedString[util.FromBytes4(codes[i-3:i+1])])
		case OP_RETURN:
			fmt.Print("<OP_RETURN>\n")
		case OP_RETURN_BLOCK:
			fmt.Print("<OP_RETURN_BLOCK>\n")
		case OP_RETURN_RESCUE:
			fmt.Print("<OP_RETURN_RESCUE>\n")
		case OP_DUP:
			fmt.Print("<OP_DUP>\n")
		case OP_NEGATE:
			fmt.Print("<OP_NEGATE>\n")
		case OP_ADD:
			fmt.Print("<OP_ADD>\n")
		case OP_SUB:
			fmt.Print("<OP_SUB>\n")
		case OP_MUL:
			fmt.Print("<OP_MUL>\n")
		case OP_DIV:
			fmt.Print("<OP_DIV>\n")
		case OP_PUSH:
			fmt.Print("<OP_PUSH>\n")
		case OP_NOP:
			fmt.Print("<OP_NOP>\n")
		case OP_POP:
			fmt.Print("<OP_POP>\n")
		case OP_CALL:
			info := (*value.CallInfo)(unsafe.Pointer(&chunk.Code[i+1]))
			i += int(unsafe.Sizeof(*info))
			fmt.Printf("<OP_CALL %s:%d>\n", info.Name, info.Arity)
		case OP_CALL_BLOCK:
			i++
			fmt.Printf("<OP_CALL_BLOCK %d>\n", codes[i])
		case OP_DOT_CALL:
			info := (*value.CallInfo)(unsafe.Pointer(&chunk.Code[i+1]))
			i += int(unsafe.Sizeof(*info))
			fmt.Printf("<OP_DOT_CALL %s:%d>\n", info.Name, info.Arity)
		case OP_OPTIONAL_CALL:
			info := (*value.CallInfo)(unsafe.Pointer(&chunk.Code[i+1]))
			i += int(unsafe.Sizeof(*info))
			fmt.Printf("<OP_OPTIONAL_CALL %s:%d>\n", info.Name, info.Arity)
		case OP_GLOBAL_METHOD:
			fmt.Print("<OP_GLOBAL_METHOD>\n")
		case OP_METHOD:
			fmt.Print("<OP_METHOD>\n")
		case OP_STATIC_METHOD:
			fmt.Printf("<OP_STATIC_METHOD>\n")
		case OP_CLASS:
			fmt.Printf("<OP_CLASS>\n")
		case OP_SET_LOCAL:
			i++
			fmt.Printf("<OP_SET_LOCAL %d>\n", codes[i])
		case OP_GET_LOCAL:
			i++
			fmt.Printf("<OP_GET_LOCAL %d>\n", codes[i])
		case OP_SET_UPVALUE:
			i++
			fmt.Printf("<OP_SET_UPVALUE %d>\n", codes[i])
		case OP_GET_UPVALUE:
			i++
			fmt.Printf("<OP_GET_UPVALUE %d>\n", codes[i])
		case OP_CLOSURE:
			i = i + 2
			index := util.FromBytes2(codes[i-1 : i+1])
			fmt.Printf("<OP_CLOSURE %d>\n", index)
			f := chunk.Constants[index].Payload.(*value.Function)
			for j := uint8(0); j < f.UpvalueCount; j++ {
				local := codes[i]
				i++
				upvalueIndex := codes[i]
				i++
				if local == 0 {
					fmt.Printf("%s  <UpValue %d>\n", spaces, upvalueIndex)
				} else {
					fmt.Printf("%s  <Local %d>\n", spaces, upvalueIndex)
				}
			}
		case OP_GET_IVAR:
			fmt.Print("<OP_GET_IVAR>\n")
		case OP_SET_IVAR:
			fmt.Print("<OP_SET_IVAR>\n")
		case OP_SET_GLOBAL:
			i = i + 2
			fmt.Printf("<OP_SET_GLOBAL %d>\n", util.FromBytes2(codes[i-1:i+1]))
		case OP_GET_GLOBAL:
			i = i + 2
			fmt.Printf("<OP_GET_GLOBAL %d>\n", util.FromBytes2(codes[i-1:i+1]))
		case OP_NIL:
			fmt.Print("<OP_NIL>\n")
		case OP_FALSE:
			fmt.Print("<OP_FALSE>\n")
		case OP_TRUE:
			fmt.Print("<OP_TRUE>\n")
		case OP_JUMP:
			i += 2
			fmt.Printf("<OP_JUMP %d>\n", util.FromBytes2(codes[i-1:i+1]))
		case OP_JUMP_IF_FALSE:
			i += 2
			fmt.Printf("<OP_JUMP_IF_FALSE %d>\n", util.FromBytes2(codes[i-1:i+1]))
		case OP_JUMP_IF_TRUE:
			i += 2
			fmt.Printf("<OP_JUMP_IF_TRUE %d>\n", util.FromBytes2(codes[i-1:i+1]))
		case OP_BRANCH_IF_FALSE:
			i += 2
			fmt.Printf("<OP_BRANCH_IF_FALSE %d>\n", util.FromBytes2(codes[i-1:i+1]))
		case OP_LOOP:
			i += 2
			fmt.Printf("<OP_LOOP -%d>\n", util.FromBytes2(codes[i-1:i+1]))
		case OP_EQUAL:
			fmt.Print("<OP_EQUAL>\n")
		case OP_GREATER_EQUAL:
			fmt.Print("<OP_GREATER_EQUAL>\n")
		case OP_LESSER_EQUAL:
			fmt.Print("<OP_LESSER_EQUAL>\n")
		case OP_LOAD_CLASS_CONST:
			fmt.Print("<OP_LOAD_CLASS_CONST>\n")
		case OP_SET_CLASS_CONST:
			fmt.Print("<OP_SET_CLASS_CONST>\n")
		case OP_NOT:
			fmt.Print("<OP_NOT>\n")
		case OP_BUILD_ARRAY:
			i++
			fmt.Printf("<OP_BUILD_ARRAY %d>\n", codes[i])
		case OP_SUBSCRIPT:
			fmt.Print("<OP_SUBSCRIPT>\n")
		case OP_SUBSCRIPT_SET:
			fmt.Print("<OP_SUBSCRIPT_SET>\n")
		case OP_BUILD_HASH:
			i++
			fmt.Printf("<OP_BUILD_HASH %d>\n", codes[i])
		case OP_INHERIT:
			fmt.Print("<OP_INHERIT>\n")
		case OP_CHECK_CLASS:
			fmt.Print("<OP_CHECK_CLASS>\n")
		case OP_RAISE:
			fmt.Print("<OP_RAISE>\n")
		case OP_SIMPLE_RAISE:
			fmt.Print("<OP_SIMPLE_RAISE>\n")
		case OP_REQUIRE_RELATIVE:
			fmt.Print("<OP_REQUIRE_RELATIVE>\n")
		default:
			panic("unknown ")
		}
	}
}
