package common_error

type SyntaxError struct {
	Message  string
	Filename string
	Line     int
}
