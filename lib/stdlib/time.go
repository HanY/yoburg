package stdlib

import (
	"time"

	"codeberg.org/HanY/yoburg/lib/constants"
	"codeberg.org/HanY/yoburg/lib/value"
)

func now(caller value.Value, v []value.Value) value.Value {
	return value.Value{Type: value.VAL_DOUBLE, Payload: float64(time.Now().UnixMicro()) / 1000_000}
}

func init() {
	c := value.NewClass("Time", constants.TIME_CLASS_ID)
	c.Constants["now"] = value.Value{Type: value.VAL_NATIVE, Payload: &value.NativeFunction{
		Func:      now,
		Name:      "now",
		ParamList: value.ParamList{Arity: 0},
	}}
	Time = c
}

var Time *value.Class
