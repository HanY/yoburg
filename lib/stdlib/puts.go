package stdlib

import "codeberg.org/HanY/yoburg/lib/value"

func puts(caller value.Value, val []value.Value) value.Value {
	if len(val) != 1 {
		panic("puts only accept one argument")
	}
	value.PrintValue(val[0])
	return value.VALUE_NIL
}

var Puts = &value.NativeFunction{
	Func:      puts,
	Name:      "puts",
	ParamList: value.ParamList{Arity: 1},
}
