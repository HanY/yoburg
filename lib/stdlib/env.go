package stdlib

import (
	"os"

	"codeberg.org/HanY/yoburg/lib/constants"
	"codeberg.org/HanY/yoburg/lib/value"
)

func getEnv(caller value.Value, v []value.Value) value.Value {
	if v[0].Type != value.VAL_STRING {
		panic("ENV only accepts string")
	}
	k := v[0].Payload.(string)
	val, ok := os.LookupEnv(k)
	if !ok {
		return value.VALUE_NIL
	}
	return value.StringVal(val)
}

func setEnv(caller value.Value, v []value.Value) value.Value {
	if v[0].Type != value.VAL_STRING || v[1].Type != value.VAL_STRING {
		panic("ENV only accepts string")
	}
	k := v[0].Payload.(string)
	val := v[1].Payload.(string)
	os.Setenv(k, val)
	return v[1]
}

func fetch(caller value.Value, v []value.Value) value.Value {
	if v[0].Type != value.VAL_STRING {
		panic("ENV only accepts string")
	}
	k := v[0].Payload.(string)
	val, ok := os.LookupEnv(k)
	if !ok {
		return v[1]
	}
	return value.StringVal(val)
}

func key(caller value.Value, v []value.Value) value.Value {
	if v[0].Type != value.VAL_STRING {
		panic("ENV only accepts string")
	}
	k := v[0].Payload.(string)
	_, ok := os.LookupEnv(k)
	if !ok {
		return value.VALUE_FALSE
	}
	return value.VALUE_TRUE
}

func init() {
	c := value.NewClass("ENV", constants.ENV_CLASS_ID)
	c.Constants["[]"] = value.Value{Type: value.VAL_NATIVE, Payload: &value.NativeFunction{
		Func:      getEnv,
		Name:      "[]",
		ParamList: value.ParamList{Arity: 1},
	}}
	c.Constants["[]="] = value.Value{Type: value.VAL_NATIVE, Payload: &value.NativeFunction{
		Func:      setEnv,
		Name:      "[]=",
		ParamList: value.ParamList{Arity: 2},
	}}
	c.Constants["key?"] = value.Value{Type: value.VAL_NATIVE, Payload: &value.NativeFunction{
		Func:      key,
		Name:      "key?",
		ParamList: value.ParamList{Arity: 1},
	}}
	c.Constants["fetch"] = value.Value{Type: value.VAL_NATIVE, Payload: &value.NativeFunction{
		Func:      fetch,
		Name:      "fetch",
		ParamList: value.ParamList{Arity: 2},
	}}
	ENV = c
}

var ENV *value.Class
