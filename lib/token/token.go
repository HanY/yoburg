package token

import (
	"fmt"
)

const (
	TOKEN_DEF uint8 = iota
	TOKEN_CLASS
	TOKEN_END
	TOKEN_IF
	TOKEN_ELSE
	TOKEN_ELSIF
	TOKEN_BEGIN
	TOKEN_DO
	TOKEN_CONTINUE
	TOKEN_FOR
	TOKEN_WHILE
	TOKEN_LAMBDA
	TOKEN_RETURN
	TOKEN_BREAK
	TOKEN_NEXT
	TOKEN_RESCUE
	TOKEN_TRUE
	TOKEN_FALSE
	TOKEN_NIL
	TOKEN_SUPER
	TOKEN_UNLESS
	TOKEN_REQUIRE
	TOKEN_REQUIRE_RELATIVE
	TOKEN_YIELD
	TOKEN_RAISE
	// /n
	TOKEN_NEWLINE
	// + - * /
	TOKEN_PLUS
	TOKEN_MINUS
	// MINUS immediately followed by token
	// we introduce this token mainly to distinguish
	// between "puts foo - bar" and "puts foo -bar"
	// we parse the former as "puts( (foo) - (bar) )"
	// latter as "puts(foo(-bar))"
	TOKEN_NEGATE_MINUS
	TOKEN_SLASH
	TOKEN_STAR
	// ( )
	TOKEN_LEFT_PAREN
	TOKEN_RIGHT_PAREN
	// { }
	TOKEN_LEFT_BRACE
	TOKEN_RIGHT_BRACE
	// .
	TOKEN_DOT
	// | ||
	TOKEN_BAR
	TOKEN_OR
	// & && &.
	TOKEN_AND
	TOKEN_LOGIC_AND
	TOKEN_OPTIONAL_CALL
	// < > = ! != <= >= ==
	TOKEN_LESS
	TOKEN_GREATER
	TOKEN_EQUAL
	TOKEN_BANG
	TOKEN_BANG_EQUAL
	TOKEN_EQUAL_EQUAL
	TOKEN_GREATER_EQUAL
	TOKEN_LESS_EQUAL
	// ?
	TOKEN_QUESTION_MARK
	// [ ]
	TOKEN_LEFT_BRACKET
	TOKEN_RIGHT_BRACKET
	// : :: :foo
	TOKEN_COLON
	TOKEN_DOUBLE_COLON
	TOKEN_SYMBOL
	// ,
	TOKEN_COMMA
	// =>
	TOKEN_ARROW
	// Literals
	TOKEN_STRING
	TOKEN_DOUBLE
	TOKEN_INT
	TOKEN_IDENTIFIER
	TOKEN_IDENTIFIER_GLOBAL
	TOKEN_IDENTIFIER_IVAR
	TOKEN_IDENTIFIER_CVAR
	TOKEN_STRING_LIST
	// Interpolation
	TOKEN_DOUBLE_QUOTE      // "
	TOKEN_INTERPO_EXP_START // #{
	// EXP_END is RIGHT_BRACE '}'

	TOKEN_EOF
)

type Token struct {
	Type  uint8
	Value interface{}
}

var EOF = Token{Type: TOKEN_EOF}

func PrintToken(token Token) {
	var str string
	switch token.Type {
	case TOKEN_DEF:
		str = "TOKEN_DEF"
	case TOKEN_CLASS:
		str = "TOKEN_CLASS"
	case TOKEN_END:
		str = "TOKEN_END"
	case TOKEN_IF:
		str = "TOKEN_IF"
	case TOKEN_ELSE:
		str = "TOKEN_ELSE"
	case TOKEN_ELSIF:
		str = "TOKEN_ELIF"
	case TOKEN_BEGIN:
		str = "TOKEN_BEGIN"
	case TOKEN_DO:
		str = "TOKEN_DO"
	case TOKEN_CONTINUE:
		str = "TOKEN_CONTINUE"
	case TOKEN_FOR:
		str = "TOKEN_FOR"
	case TOKEN_WHILE:
		str = "TOKEN_WHILE"
	case TOKEN_LAMBDA:
		str = "TOKEN_LAMBDA"
	case TOKEN_RETURN:
		str = "TOKEN_RETURN"
	case TOKEN_BREAK:
		str = "TOKEN_BREAK"
	case TOKEN_NEXT:
		str = "TOKEN_NEXT"
	case TOKEN_RESCUE:
		str = "TOKEN_RESCUE"
	case TOKEN_TRUE:
		str = "TOKEN_TRUE"
	case TOKEN_FALSE:
		str = "TOKEN_FALSE"
	case TOKEN_NIL:
		str = "TOKEN_NIL"
	case TOKEN_SUPER:
		str = "TOKEN_SUPER"
	case TOKEN_UNLESS:
		str = "TOKEN_UNLESS"
	case TOKEN_REQUIRE:
		str = "TOKEN_REQUIRE"
	case TOKEN_REQUIRE_RELATIVE:
		str = "TOKEN_REQUIRE_RELATIVE"
	case TOKEN_NEWLINE:
		str = "TOKEN_NEWLINE"
	case TOKEN_PLUS:
		str = "TOKEN_PLUS"
	case TOKEN_MINUS:
		str = "TOKEN_MINUS"
	case TOKEN_NEGATE_MINUS:
		str = "TOKEN_NEGATE_MINUS"
	case TOKEN_SLASH:
		str = "TOKEN_SLASH"
	case TOKEN_YIELD:
		str = "TOKEN_YIELD"
	case TOKEN_STAR:
		str = "TOKEN_STAR"
	case TOKEN_LEFT_PAREN:
		str = "TOKEN_LEFT_PAREN"
	case TOKEN_RIGHT_PAREN:
		str = "TOKEN_RIGHT_PAREN"
	case TOKEN_LEFT_BRACE:
		str = "TOKEN_LEFT_BRACE"
	case TOKEN_RIGHT_BRACE:
		str = "TOKEN_RIGHT_BRACE"
	case TOKEN_DOUBLE_QUOTE:
		str = "TOKEN_DOUBLE_QUOTE"
	case TOKEN_DOT:
		str = "TOKEN_DOT"
	case TOKEN_OPTIONAL_CALL:
		str = "TOKEN_OPTIONAL_CALL"
	case TOKEN_BAR:
		str = "TOKEN_BAR"
	case TOKEN_OR:
		str = "TOKEN_OR"
	case TOKEN_AND:
		str = "TOKEN_AND"
	case TOKEN_LOGIC_AND:
		str = "TOKEN_LOGIC_AND"
	case TOKEN_LESS:
		str = "TOKEN_LESS"
	case TOKEN_GREATER:
		str = "TOKEN_GREATER"
	case TOKEN_EQUAL:
		str = "TOKEN_EQUAL"
	case TOKEN_BANG:
		str = "TOKEN_BANG"
	case TOKEN_BANG_EQUAL:
		str = "TOKEN_BANG_EQUAL"
	case TOKEN_EQUAL_EQUAL:
		str = "TOKEN_EQUAL_EQUAL"
	case TOKEN_GREATER_EQUAL:
		str = "TOKEN_GREATER_EQUAL"
	case TOKEN_LESS_EQUAL:
		str = "TOKEN_LESS_EQUAL"
	case TOKEN_QUESTION_MARK:
		str = "TOKEN_QUESTION_MARK"
	case TOKEN_LEFT_BRACKET:
		str = "TOKEN_LEFT_BRACKET"
	case TOKEN_RIGHT_BRACKET:
		str = "TOKEN_RIGHT_BRACKET"
	case TOKEN_COLON:
		str = "TOKEN_COLON"
	case TOKEN_DOUBLE_COLON:
		str = "TOKEN_DOUBLE_COLON"
	case TOKEN_SYMBOL:
		str = fmt.Sprintf("TOKEN_SYMBOL #%s", token.Value.([]byte))
	case TOKEN_COMMA:
		str = "TOKEN_COMMA"
	case TOKEN_ARROW:
		str = "TOKEN_ARROW"
	case TOKEN_STRING:
		str = fmt.Sprintf("TOKEN_STRING #%s", token.Value.(string))
	case TOKEN_DOUBLE:
		str = fmt.Sprintf("TOKEN_DOUBLE #%f", token.Value.(float64))
	case TOKEN_INT:
		str = fmt.Sprintf("TOKEN_INT #%d", token.Value.(int))
	case TOKEN_IDENTIFIER:
		str = fmt.Sprintf("TOKEN_IDENTIFIER #%s", string(token.Value.([]byte)))
	case TOKEN_IDENTIFIER_GLOBAL:
		str = fmt.Sprintf("TOKEN_IDENTIFIER_GLOBAL #%s", string(token.Value.([]byte)))
	case TOKEN_IDENTIFIER_IVAR:
		str = fmt.Sprintf("TOKEN_IDENTIFIER_IVAR #%s", string(token.Value.([]byte)))
	case TOKEN_IDENTIFIER_CVAR:
		str = fmt.Sprintf("TOKEN_IDENTIFIER_CVAR #%s", string(token.Value.([]byte)))
	case TOKEN_STRING_LIST:
		str = fmt.Sprintf("TOKEN_STRING_LIST #%s", token.Value.([]string))
	case TOKEN_INTERPO_EXP_START:
		str = "TOKEN_INTERPO_EXP_START"
	case TOKEN_RAISE:
		str = "TOKEN_RAISE"
	case TOKEN_EOF:
		str = "TOKEN_EOF"
	default:
		panic("unrecognized token")
	}
	fmt.Printf("<%s>\n", str)
}
