package util

import (
	"encoding/binary"
)

func ToBytes2(v uint16) []uint8 {
	ret := make([]byte, 2)
	binary.LittleEndian.PutUint16(ret, v)
	return ret
}

func ToBytes4(v uint32) []uint8 {
	ret := make([]byte, 4)
	binary.LittleEndian.PutUint32(ret, v)
	return ret
}

func ToBytes8(v uint64) []uint8 {
	ret := make([]byte, 8)
	binary.LittleEndian.PutUint64(ret, v)
	return ret
}

func FromBytes2(b []uint8) uint16 {
	return binary.LittleEndian.Uint16(b)
}

func FromBytes4(b []uint8) uint32 {
	return binary.LittleEndian.Uint32(b)
}

func FromBytes8(b []uint8) uint64 {
	return binary.LittleEndian.Uint64(b)
}
