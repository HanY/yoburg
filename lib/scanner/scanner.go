package scanner

import (
	"bytes"
	"io"
	"log"
	"slices"
	"strings"

	"codeberg.org/HanY/yoburg/lib/token"
)

type Scanner struct {
	buf         []byte
	cursor      int
	currentLine int
	tokenBuf    []token.Token
}

func NewScanner(source io.Reader) *Scanner {
	buf, err := io.ReadAll(source)
	if err != nil {
		log.Panicf("failed to read source file: %v", err)
	}
	return &Scanner{buf: buf, currentLine: 1}
}

func (s *Scanner) Line() int {
	return s.currentLine
}

func (s *Scanner) isEnd() bool {
	return s.cursor == len(s.buf)
}

func (s *Scanner) peek(t uint8) bool {
	return s.buf[s.cursor] == t
}

func (s *Scanner) Scan() token.Token {
	if len(s.tokenBuf) > 0 {
		v := s.tokenBuf[len(s.tokenBuf)-1]
		s.tokenBuf = s.tokenBuf[:len(s.tokenBuf)-1]
		return v
	}
	if s.skipWhitespace() {
		return token.Token{Type: token.TOKEN_NEWLINE}
	}

	if s.cursor == len(s.buf) {
		return token.EOF
	}

	c := s.buf[s.cursor]
	if isAlpha(c) {
		return s.identifier()
	}

	if isDigit(c) {
		return s.number()
	}

	s.cursor++
	switch c {
	case '(':
		return token.Token{Type: token.TOKEN_LEFT_PAREN}
	case ')':
		return token.Token{Type: token.TOKEN_RIGHT_PAREN}
	case '{':
		return token.Token{Type: token.TOKEN_LEFT_BRACE}
	case '}':
		return token.Token{Type: token.TOKEN_RIGHT_BRACE}
	// case ';':
	// 	return token.Token{Type: token.TOKEN_SEMICOLON}
	case ',':
		return token.Token{Type: token.TOKEN_COMMA}
	case '.':
		return token.Token{Type: token.TOKEN_DOT}
	case '-':
		if !s.isEnd() {
			if s.peek('>') {
				s.cursor++
				return token.Token{Type: token.TOKEN_LAMBDA}
			} else if !s.peek(' ') && !s.peek('\r') && !s.peek('\n') && !s.peek('\t') {
				return token.Token{Type: token.TOKEN_NEGATE_MINUS}
			}
		}
		return token.Token{Type: token.TOKEN_MINUS}
	case '+':
		return token.Token{Type: token.TOKEN_PLUS}
	case '/':
		return token.Token{Type: token.TOKEN_SLASH}
	case '*':
		return token.Token{Type: token.TOKEN_STAR}
	case '?':
		return token.Token{Type: token.TOKEN_QUESTION_MARK}
	case '[':
		return token.Token{Type: token.TOKEN_LEFT_BRACKET}
	case ']':
		return token.Token{Type: token.TOKEN_RIGHT_BRACKET}
		//> two-char
	case ':':
		if !s.isEnd() && (s.buf[s.cursor] == '@' || isAlpha(s.buf[s.cursor])) {
			start := s.cursor
			s.cursor++
			for !s.isEnd() && isAlpha(s.buf[s.cursor]) {
				s.cursor++
			}
			return token.Token{Type: token.TOKEN_SYMBOL, Value: s.buf[start:s.cursor]}
		}
		if !s.isEnd() && s.peek(':') {
			s.cursor++
			return token.Token{Type: token.TOKEN_DOUBLE_COLON}
		}
		return token.Token{Type: token.TOKEN_COLON}

	case '!':
		return s.matchType('=', token.TOKEN_BANG_EQUAL, token.TOKEN_BANG)
	case '=':
		if s.cursor == len(s.buf) {
			return token.Token{Type: token.TOKEN_EQUAL}
		}
		if s.buf[s.cursor] == '=' {
			s.cursor++
			return token.Token{Type: token.TOKEN_EQUAL_EQUAL}
		}
		if s.buf[s.cursor] == '>' {
			s.cursor++
			return token.Token{Type: token.TOKEN_ARROW}
		}
		return token.Token{Type: token.TOKEN_EQUAL}
	case '<':
		if s.isEnd() {
			return token.Token{Type: token.TOKEN_LESS}
		}
		c = s.buf[s.cursor]
		s.cursor++
		switch c {
		case '<':
			return s.parseHeredoc()
		case '=':
			return token.Token{Type: token.TOKEN_LESS_EQUAL}
		default:
			s.cursor--
			return token.Token{Type: token.TOKEN_LESS}
		}
	case '>':
		return s.matchType('=', token.TOKEN_GREATER_EQUAL, token.TOKEN_GREATER)
	case '|':
		return s.matchType('|', token.TOKEN_OR, token.TOKEN_BAR)
	case '&':
		if s.isEnd() {
			return token.Token{Type: token.TOKEN_AND}
		}
		switch s.buf[s.cursor] {
		case '&':
			s.cursor++
			return token.Token{Type: token.TOKEN_LOGIC_AND}
		case '.':
			s.cursor++
			return token.Token{Type: token.TOKEN_OPTIONAL_CALL}
		default:
			return token.Token{Type: token.TOKEN_AND}
		}
	case '$':
		return s.global()
	case '\'':
		return s.rawString()
	case '@':
		return s.identifierVar()
	case '%':
		if !s.isEnd() && s.peek('w') {
			s.cursor++
			if !s.isEnd() && s.peek('[') {
				s.cursor++
				return s.parseStringList()
			}
		}
	case '"':
		s.tokenBuf = append(s.tokenBuf, s.parseStringInterpolation()...)
		return token.Token{Type: token.TOKEN_DOUBLE_QUOTE}
	}

	log.Panic("Invalid token")
	return token.Token{}
}

func (s *Scanner) matchString(str string) bool {
	i := 0
	oldCur := s.cursor
	for !s.isEnd() && i < len(str) {
		if !s.peek(str[i]) {
			return false
		}
		s.cursor++
		i++
	}

	if i == len(str) {
		return true
	} else {
		s.cursor = oldCur
		return false
	}
}

func (s *Scanner) parseHeredoc() token.Token {
	indentEnd := false
	indentAll := false

	if !s.isEnd() {
		switch s.buf[s.cursor] {
		case '-':
			indentEnd = true
			s.cursor++
		case '~':
			indentAll = true
			s.cursor++
		}
	}

	b := strings.Builder{}
	var id string
	for !s.isEnd() && !s.peek('\n') {
		b.WriteByte(s.buf[s.cursor])
		s.cursor++
	}
	if s.isEnd() {
		panic("unexpected EOF")
	}

	// skip the newline after HEREDOC
	s.currentLine++
	s.cursor++
	id = b.String()
	b.Reset()
	isLineStart := true
	for !s.isEnd() && !s.matchString(id) {
		if isLineStart && indentAll {
			// skip indent in the front
			for !s.isEnd() {
				s.cursor++
				c := s.buf[s.cursor]
				if c == ' ' || c == '\r' || c == '\t' {
					continue
				}
				break
			}
			isLineStart = false
			continue
		}
		if s.peek('\n') {
			s.currentLine++
			isLineStart = true
		}
		b.WriteByte(s.buf[s.cursor])
		s.cursor++
	}
	val := b.String()

	if indentEnd && !indentAll {
		i := s.cursor - 1 - len(id)
		for s.buf[i] != '\n' {
			i--
		}
		val = val[:len(val)-(s.cursor-1-i-len(id))]
	}

	return token.Token{Type: token.TOKEN_STRING, Value: val}
}

func (s *Scanner) parseStringInterpolation() []token.Token {
	var tokens []token.Token
	for !s.isEnd() && !s.peek('"') {
	START:
		b := strings.Builder{}
		for !s.isEnd() && !s.peek('"') {
			if s.peek('#') {
				s.cursor++
				if !s.isEnd() && s.peek('{') {
					s.cursor++
					tokens = append(tokens,
						token.Token{Type: token.TOKEN_STRING, Value: b.String()},
						token.Token{Type: token.TOKEN_INTERPO_EXP_START})
					tokens = append(tokens, s.parseInterpo()...)
					goto START
				}
			}
			if s.buf[s.cursor] == '\\' {
				s.cursor++
				switch s.buf[s.cursor] {
				case 'n':
					b.WriteByte('\n')
				case 't':
					b.WriteByte('\t')
				case '"':
					b.WriteByte('"')
				default:
					b.WriteByte(s.buf[s.cursor])
				}
			} else {
				b.WriteByte(s.buf[s.cursor])
			}
			s.cursor++
		}
		if b.Len() > 0 {
			// don't parse "#{exp}" as 'exp + empty_string'
			tokens = append(tokens, token.Token{Type: token.TOKEN_STRING, Value: b.String()})
		}
	}
	if !s.isEnd() {
		if len(tokens) == 0 {
			// parse "" as double_quote + empty_string + double_quote
			tokens = append(tokens, token.Token{Type: token.TOKEN_STRING, Value: ""})
		}
		tokens = append(tokens, token.Token{Type: token.TOKEN_DOUBLE_QUOTE})
		s.cursor++
	} else {
		panic("unexpected EOF, expect \"")
	}
	slices.Reverse(tokens)
	return tokens
}

func (s *Scanner) parseInterpo() []token.Token {
	var tokens []token.Token
	var t token.Token
	leftBrace := 1
	for {
		t = s.Scan()
		tokens = append(tokens, t)
		switch t.Type {
		case token.TOKEN_EOF:
			goto RETURN
		case token.TOKEN_INTERPO_EXP_START:
			leftBrace++
		case token.TOKEN_RIGHT_BRACE:
			leftBrace--
			if leftBrace == 0 {
				goto RETURN
			}
		}
	}
RETURN:
	return tokens
}

func (s *Scanner) parseStringList() token.Token {
	var list []string
	for !s.isEnd() {
		// skip whitespace
		for !s.isEnd() && isWhitespace(s.buf[s.cursor]) {
			s.cursor++
		}
		if s.peek(']') {
			s.cursor++
			return token.Token{Type: token.TOKEN_STRING_LIST, Value: list}
		}
		start := s.cursor
		for !s.isEnd() && !isWhitespace(s.buf[s.cursor]) && !s.peek(']') {
			s.cursor++
		}
		list = append(list, string(s.buf[start:s.cursor]))
	}
	panic("unexpected EOF, expect ']'")
}

func isAlpha(c byte) bool {
	return (c >= 'a' && c <= 'z') ||
		(c >= 'A' && c <= 'Z') ||
		c == '_'
}

func isDigit(c byte) bool {
	return c >= '0' && c <= '9'
}

func (s *Scanner) matchType(c byte, matchedType uint8, missmatchedType uint8) token.Token {
	if s.isEnd() {
		return token.Token{Type: missmatchedType}
	}
	if s.buf[s.cursor] != c {
		return token.Token{Type: missmatchedType}
	}
	s.cursor++
	return token.Token{Type: matchedType}
}

func isWhitespace(b byte) bool {
	if b == ' ' || b == '\n' || b == '\r' || b == '\t' {
		return true
	}
	return false
}

// skipWhitespace: return true if newline
func (s *Scanner) skipWhitespace() bool {
	newline := false
	for s.cursor < len(s.buf) {
		c := s.buf[s.cursor]
		switch c {
		case ' ':
			fallthrough
		case '\r':
			fallthrough
		case '\t':
			s.cursor++
		case '\n':
			s.currentLine++
			s.cursor++
			newline = true
		case '#':
			// A comment goes until the end of the line.
			for !s.isEnd() {
				s.cursor++
				if s.buf[s.cursor] == '\n' {
					for !s.isEnd() && s.buf[s.cursor] == '\n' {
						s.cursor++
						s.currentLine++
					}
					break
				}
			}
		default:
			return newline
		}
	}
	return newline
}

func (s *Scanner) number() token.Token {
	integer := 0
	for s.cursor != len(s.buf) && isDigit(s.buf[s.cursor]) {
		integer = integer*10 + int(s.buf[s.cursor]) - '0'
		s.cursor++
	}
	if s.cursor+1 < len(s.buf) && s.buf[s.cursor] == '.' && isDigit(s.buf[s.cursor]) {
		s.cursor++
		doubleValue := float64(integer)
		var position float64 = 10
		for s.cursor != len(s.buf) && isDigit(s.buf[s.cursor]) {
			doubleValue += float64(s.buf[s.cursor]-'0') / position
			position *= 10
			s.cursor++
		}
		return token.Token{Type: token.TOKEN_DOUBLE, Value: doubleValue}
	}
	return token.Token{Type: token.TOKEN_INT, Value: integer}
}

func (s *Scanner) rawString() token.Token {
	start := s.cursor
	for !s.isEnd() && s.buf[s.cursor] != '\'' {
		c := s.buf[s.cursor]
		if c == '\n' {
			s.currentLine++
		}
		if c == '\\' && s.cursor+1 != len(s.buf) {
			s.cursor++
		}
		s.cursor++
	}
	if s.cursor == len(s.buf) {
		return token.EOF
	}

	// skip last '
	s.cursor++
	return token.Token{Type: token.TOKEN_STRING, Value: string(s.buf[start : s.cursor-1])}
}

func (s *Scanner) global() token.Token {
	start := s.cursor
	c := s.buf[s.cursor]
	if s.cursor != len(s.buf) {
		// from https://docs.ruby-lang.org/en/2.6.0/globals_rdoc.html
		switch c {
		case '!':
			fallthrough
		case '@':
			fallthrough
		case '&':
			fallthrough
		case '`':
			fallthrough
		case '\'':
			fallthrough
		case '+':
			fallthrough
		case '~':
			fallthrough
		case '=':
			fallthrough
		case '/':
			fallthrough
		case '\\':
			fallthrough
		case ',':
			fallthrough
		case ';':
			fallthrough
		case '.':
			fallthrough
		case '<':
			fallthrough
		case '>':
			fallthrough
		case '_':
			fallthrough
		case '*':
			fallthrough
		case '$':
			fallthrough
		case '?':
			fallthrough
		case ':':
			fallthrough
		case '"':
			s.cursor++
			return token.Token{Type: token.TOKEN_IDENTIFIER_GLOBAL, Value: []byte{s.buf[s.cursor+1]}}
		case '-':
			if s.cursor+1 != len(s.buf) {
				tmp := s.buf[s.cursor+1]
				s.cursor = s.cursor + 2
				switch tmp {
				case '0':
					return token.Token{Type: token.TOKEN_IDENTIFIER_GLOBAL, Value: []byte{'/'}}
				case 'a':
					return token.Token{Type: token.TOKEN_IDENTIFIER_GLOBAL, Value: []byte("-a")}
				case 'd':
					return token.Token{Type: token.TOKEN_IDENTIFIER_GLOBAL, Value: []byte("DEBUG")}
				case 'F':
					return token.Token{Type: token.TOKEN_IDENTIFIER_GLOBAL, Value: []byte{';'}}
				case 'i':
					return token.Token{Type: token.TOKEN_IDENTIFIER_GLOBAL, Value: []byte("-i")}
				case 'I':
					return token.Token{Type: token.TOKEN_IDENTIFIER_GLOBAL, Value: []byte{':'}}
				case 'l':
					return token.Token{Type: token.TOKEN_IDENTIFIER_GLOBAL, Value: []byte("-l")}
				case 'p':
					return token.Token{Type: token.TOKEN_IDENTIFIER_GLOBAL, Value: []byte("-p")}
				case 'v':
					fallthrough
				case 'w':
					return token.Token{Type: token.TOKEN_IDENTIFIER_GLOBAL, Value: []byte("VERBOSE")}
				default:
					log.Panic("Invalid global token name")
				}
			}
		}
	}

	for s.cursor != len(s.buf) && (isAlpha(s.buf[s.cursor]) || isDigit(s.buf[s.cursor])) {
		s.cursor++
	}
	if start == s.cursor {
		log.Panic("variable without name")
	}
	return token.Token{Type: token.TOKEN_IDENTIFIER_GLOBAL, Value: s.buf[start:s.cursor]}
}

func (s *Scanner) identifierVar() token.Token {
	idType := token.TOKEN_IDENTIFIER_IVAR
	if s.cursor+1 != len(s.buf) && s.buf[s.cursor] == '@' {
		idType = token.TOKEN_IDENTIFIER_CVAR
		s.cursor++
	}
	s.cursor++
	start := s.cursor

	for s.cursor != len(s.buf) && (isAlpha(s.buf[s.cursor]) || isDigit(s.buf[s.cursor]) || s.buf[s.cursor] == '?' || s.buf[s.cursor] == '!') {
		s.cursor++
	}

	return token.Token{Type: idType, Value: s.buf[start:s.cursor]}
}

func (s *Scanner) identifier() token.Token {
	idType := token.TOKEN_IDENTIFIER
	c := s.buf[s.cursor]
	if c == '@' {
		idType = token.TOKEN_IDENTIFIER_IVAR
		if s.cursor+1 != len(s.buf) && s.buf[s.cursor] == '@' {
			idType = token.TOKEN_IDENTIFIER_CVAR
			s.cursor++
		}
		s.cursor++
	}

	start := s.cursor

	for s.cursor != len(s.buf) && (isAlpha(s.buf[s.cursor]) || isDigit(s.buf[s.cursor]) || s.buf[s.cursor] == '?' || s.buf[s.cursor] == '!') {
		s.cursor++
	}

	id := s.buf[start:s.cursor]
	switch id[0] {
	case 'b':
		if bytes.Equal(id, []byte("begin")) {
			return token.Token{Type: token.TOKEN_BEGIN}
		}
	case 'c':
		if len(id) >= 5 {
			switch id[1] {
			case 'l':
				if bytes.Equal(id, []byte("class")) {
					return token.Token{Type: token.TOKEN_CLASS}
				}
			case 'o':
				if bytes.Equal(id, []byte("continue")) {
					return token.Token{Type: token.TOKEN_CONTINUE}
				}
			}
		}
	case 'd':
		if len(id) >= 2 {
			switch id[1] {
			case 'o':
				if bytes.Equal(id, []byte("do")) {
					return token.Token{Type: token.TOKEN_DO}
				}
			case 'e':
				if bytes.Equal(id, []byte("def")) {
					return token.Token{Type: token.TOKEN_DEF}
				}
			}
		}
	case 'e':
		if len(id) >= 3 {
			switch id[1] {
			case 'n':
				if bytes.Equal(id, []byte("end")) {
					return token.Token{Type: token.TOKEN_END}
				}
			case 'l':
				if bytes.Equal(id, []byte("else")) {
					return token.Token{Type: token.TOKEN_ELSE}
				} else if bytes.Equal(id, []byte("elsif")) {
					return token.Token{Type: token.TOKEN_ELSIF}
				}
			}
		}
	case 'f':
		if len(id) >= 3 {
			switch id[1] {
			case 'a':
				if bytes.Equal(id, []byte("false")) {
					return token.Token{Type: token.TOKEN_FALSE}
				}
			case 'o':
				if bytes.Equal(id, []byte("for")) {
					return token.Token{Type: token.TOKEN_FOR}
				}
			}
		}
	case 'i':
		if bytes.Equal(id, []byte("if")) {
			return token.Token{Type: token.TOKEN_IF}
		}
	case 'l':
		if bytes.Equal(id, []byte("lambda")) {
			return token.Token{Type: token.TOKEN_LAMBDA}
		}
	case 'n':
		if bytes.Equal(id, []byte("nil")) {
			return token.Token{Type: token.TOKEN_NIL}
		}
	case 'r':
		if len(id) >= 5 {
			switch id[1] {
			case 'a':
				if bytes.Equal(id, []byte("raise")) {
					return token.Token{Type: token.TOKEN_RAISE}
				}
			case 'e':
				switch id[2] {
				case 'q':
					if bytes.Equal(id, []byte("require")) {
						return token.Token{Type: token.TOKEN_REQUIRE}
					} else if bytes.Equal(id, []byte("require_relative")) {
						return token.Token{Type: token.TOKEN_REQUIRE_RELATIVE}
					}
				case 's':
					if bytes.Equal(id, []byte("rescue")) {
						return token.Token{Type: token.TOKEN_RESCUE}
					}
				case 't':
					if bytes.Equal(id, []byte("return")) {
						return token.Token{Type: token.TOKEN_RETURN}
					}
				}
			}
		}
	case 's':
		if bytes.Equal(id, []byte("super")) {
			return token.Token{Type: token.TOKEN_SUPER}
		}
	case 't':
		if bytes.Equal(id, []byte("true")) {
			return token.Token{Type: token.TOKEN_TRUE}
		}
	case 'u':
		if bytes.Equal(id, []byte("unless")) {
			return token.Token{Type: token.TOKEN_UNLESS}
		}
	case 'w':
		if bytes.Equal(id, []byte("while")) {
			return token.Token{Type: token.TOKEN_WHILE}
		}
	case 'y':
		if bytes.Equal(id, []byte("yield")) {
			return token.Token{Type: token.TOKEN_YIELD}
		}
	}
	return token.Token{Type: idType, Value: id}
}
