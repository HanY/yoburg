# Yoburg
A ruby like language implemented in pure go. 
It's intended to be an embedded scripting language.

## TODO
Yoburg is still in early development, the following features are on the agenda.

* Standard library
* 'require' and 'require_relative'
* Stack unwinding
* Better error display and debugging